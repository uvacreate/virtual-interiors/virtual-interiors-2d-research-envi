/**
* @file Entry point for Virtual Interiors 2D Research Environment demonstrator (maps)
* @version 1.0
* @author Hugo Huurdeman
* @copyright GPLv3 license
*/

import 'ol/ol.css';
import Map from 'ol/Map';
import View from 'ol/View';
import GeoJSON from 'ol/format/GeoJSON.js';
import TileLayer from 'ol/layer/Tile';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import Polygon from 'ol/geom/Polygon';
import MultiPolygon from 'ol/geom/MultiPolygon';
window.polies = featureArrayPolygons;
window.Feature = Feature;
window.Point = Point;
window.Feature = Feature;
window.Point = Point;
window.Polygon=Polygon;
import VectorLayer from 'ol/layer/Vector.js';
import VectorSource from 'ol/source/Vector.js';
window.VectorSource = VectorSource;
window.VectorLayerProto = VectorLayer;

//dragbox
import { DragBox, Select } from 'ol/interaction';
import { platformModifierKeyOnly, click } from 'ol/events/condition';

//Overlay
import { Overlay } from 'ol';

import { Style } from 'ol/style.js';

//The XYZ source is used for tile data that is accessed through URLs that include a zoom level and tile grid x/y coordinates.
import XYZSource from 'ol/source/XYZ';
import { getArea } from 'ol/sphere';

//use bootstrap for front-end layout
import 'bootstrap';
//load custom bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';
//custom css, overwriting some of bootstrap's default values
import './data/css/main-style.css';

//import jquery to more easily bind actions to e.g. buttons
var $ = require('jquery');

//multiselect spatial points
//a normal select interaction to handle click (ignores "normal" clicks since handled separately)
//[todo] merge click and multiple selection features into one function
var select = new Select({ condition: platformModifierKeyOnly, style: false });
// a DragBox interaction used to select features by drawing boxes
var dragBox = new DragBox({
  condition: platformModifierKeyOnly
});

//main map variable
var map;

//main map variable
let clicked = false;

//Tiles data URLs
const TILEMAP_CREATE_URL_PREFIX = 'https://images.huygens.knaw.nl/webmapper/maps/';
//ATM maps use .png tiles
const TILEMAP_CREATE_URL_SUFFIX = '/{z}/{x}/{y}.png';
//Except for ATM Loman map, uses .jpeg
const TILEMAP_CREATE_URL_LOMAN_SUFFIX = '/{z}/{x}/{y}.jpeg';
//Adamlink data URLs
const GEOJSON_ADAMLINK_LOCAL_URL = 'data/geojson/adamnet/';
const GEOJSON_ADAMLINK_LOCAL_URL_SUFFIX = '.json';
//Perhaps not all streets maps are valid geojson..
const GEOJSON_ADAMLINK_STREETSPERYEAR_URL = 'https://adamlink.nl/data/geojson/streetsperyear/';

//various helper functions for input data (CSV/parameter)
var inputDataHelper = new InputDataHelper();
import DataStorage from './data/js/data-storage.js';

//browser parameters and default values
//set constants according to browser parameters, or use default value (Amsterdam)
const SHOW_POLYGONS = inputDataHelper.checkParameter('polygons', false, inputDataHelper.parseBoolean);
const POLYGON_UNCERTAINTY = inputDataHelper.checkParameter('polygonuncertainty', true, inputDataHelper.parseBoolean);
const DEFAULT_LON = inputDataHelper.checkParameter('lon', 4.892540077, parseFloat);
const DEFAULT_LAT = inputDataHelper.checkParameter('lat', 52.373078399, parseFloat);
const DEFAULT_ZOOM = inputDataHelper.checkParameter('zoom', 14.8, parseInt);
const DEFAULT_ROTATION = inputDataHelper.checkParameter('rotation', Math.PI / 1.31, parseInt);

//constants for map, map animation
const MIN_YEAR = inputDataHelper.checkParameter('min', 1585, parseInt);
const MAX_YEAR = inputDataHelper.checkParameter('max', 1700, parseInt);
const YEAR_RATE = inputDataHelper.checkParameter('rate', 10, parseInt); // years per second
const DEFAULT_YEAR = inputDataHelper.checkParameter('year', 1635, parseInt);
const YEAR_RANGE = inputDataHelper.checkParameter('range', 10, parseInt);

//constant indicating if code (CesiumJS) for 3D map should be loaded
const ENABLE_3D_MAP = inputDataHelper.checkParameter('enable3d', false, Boolean)

//Should the application open a dataset by default, that can be specified here
var DEFAULT_DATASET_URL = 'data/csv/dataset.csv';

//const with dataset url and descr (shown in UI)
const DATASET_URL = inputDataHelper.checkParameter('data', DEFAULT_DATASET_URL, inputDataHelper.checkDatasetURL);
//constant with (readable) name of dataset
const DATASET_LABEL = inputDataHelper.checkParameter('label', DATASET_URL, String)

//import helper classes
import MapsYearHelper from './data/js/maps-year-helper.js';
var mapsYearHelper = new MapsYearHelper(MIN_YEAR, MAX_YEAR, DEFAULT_YEAR, YEAR_RATE, YEAR_RANGE);
import MapsVectorDataLayer from './data/js/maps-vector-data-layer.js';
var mapsVectorDataLayer = new MapsVectorDataLayer();
import MapsStyling from './data/js/maps-styling.js';
var mapsStyling = new MapsStyling(mapsVectorDataLayer, POLYGON_UNCERTAINTY);
import MapsDataHelper from './data/js/maps-data-helper.js';
var mapsDataHelper = new MapsDataHelper();
import InputDataHelper from './data/js/input-data-helper.js';
var dataStorage = new DataStorage();
window.polies = [];
import Maps2DGUI from './data/js/maps-2d-gui.js';
var maps2DGUI = new Maps2DGUI(select, mapsVectorDataLayer, mapsStyling, mapsYearHelper, dataStorage);
import Maps3DGUI from './data/js/maps-3d-gui.js';
var maps3DGUI;

// Create the tooltip element
const tooltip = document.getElementById('mapToolTip');

//dataset variables
var featureArrayPoints = [];
var featureArrayPolygons = [];

//animation properties
var animationIsPlaying = false;
//current time variable, initialized when initiating
var animationStartTime;

/* ************************ */
/* ** animation features ** */
/**
  * @desc Start playing years animation
  * @todo Decide on behavior in terms of year to start from (now always earliest data point year)
*/
function playYearAnimation() {
  animationIsPlaying = true;
  //always start from first defined year for the moment
  mapsYearHelper.setCurrentYearToFirstYear();
  //always use default (10y) range
  mapsYearHelper.setYearRangeToDefaultRange();
  animationStartTime = Date.now();
  renderAnimation();
}

/**
  * @desc Start the animation rendering
  * @todo merge functionality with other functions?
*/
function renderAnimation() {
  if (animationIsPlaying) {
    renderAnimationFrame();
  }
}

/**
  * @desc Render a frame of the years animation
  * @todo Decide on behavior in terms of year to start from (now always earliest data point year)
*/
function renderAnimationFrame() {
  if (mapsYearHelper.currentYear < mapsYearHelper.maxYear - 1) {
    //animate years
    const elapsed = mapsYearHelper.yearRate * (Date.now() - animationStartTime) / 2000;
    mapsYearHelper.currentYear = (mapsYearHelper.minYear + (elapsed % mapsYearHelper.yearSpan));
    maps2DGUI.timeSliderLabel = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
    updateDataLayer();
  } else {
    animationIsPlaying = false;
  }
}

/**
  * @desc Update the data layer shown (for animation purposes)
  * @todo check division of function with updateCurrentDataLayer()
*/
function updateDataLayer() {
  map.render();
  //update 
  maps2DGUI.timeSliderCurrentYear = mapsYearHelper.currentYear;
  requestAnimationFrame(renderAnimation);

  //completeFeatureArrayForCurrentVectorDataLayer, global var, initiated when setting vectorDataLayer
  updateCurrentDataLayer('vectorDataLayer', mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);

  //update textual list (fixme: doing filtering two times, above, and here)
  var currentlyFilteredItemsArr = mapsYearHelper.getFeatureArrayForYearRange(mapsYearHelper.getStartYearOfRange(), mapsYearHelper.getEndYearOfRange(), mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);

  maps2DGUI.createTextualPointsList(currentlyFilteredItemsArr);
  maps2DGUI.searchObjectList(maps2DGUI.getCurrentSearchFilter());
}
/* ** end animation features ** */
/* **************************** */


/* ** *************************** ** */
/* ** dataset processing features ** */
/**
  * @desc Create generic data object for a feature (applicable to different datasets)
  * @param various 
  * @returns feature {Feature} Feature obj with geometry (geometryPoint and/or geometryWKT) and additional textual properties. Datasetdescr optional.
*/
function createFeatureObject(id, lon, lat, wkt, type, name, startYear, endYear, picartaUrl, occupationYear, address, processedAddress, category, ecarticoID, datasetDescr, immigrant, birthYear) {
  var feature = new Feature({});
  var areaSize;
  var hasKnownLocationMidPoint = false;

  if (wkt) {
    if (wkt.type == 'POLYGON') {
      feature.set('geometryWKT', new Polygon(wkt.coordinates));
      //var hasKnownLocation = true;
    } else if (wkt.type == 'MULTIPOLYGON') {
      feature.set('geometryWKT', new MultiPolygon(wkt.coordinates));
      //var hasKnownLocation = true;
    } else {
      console.log('[err] Currently unknown wkt type specified');
      //return null;
    }
  }

  if (lon != '' && lat != '' && lon != null && lat != null) {
    feature.set('geometryPoint', new Point(mapsDataHelper.fromLonLat([lon, lat])));
    var hasKnownLocationMidPoint = true;
  }

  if(hasKnownLocationMidPoint) {
    // Render the feature as a point using the coordinates from geometryPoint
    maps2DGUI.setFeatureGeometryAsPoint(feature);
    areaSize = getArea(feature.getGeometry()).toFixed(0);
  }

  feature.setProperties({ 'hasKnownLocation': hasKnownLocationMidPoint, 'id': id, 'type': type, 'name': name, 'startYear': startYear, 'endYear': endYear, 'picartaUrl': picartaUrl, 'occupationYear': occupationYear, 'address': address, 'processedAddress': processedAddress, 'category': category, 'ecarticoID': ecarticoID, 'areaSize': areaSize, datasetDescription: datasetDescr, 'immigrant': immigrant, 'birthYear': birthYear });

  return feature;
}

/**
  * @desc Create an array with artist / publisher Features, for a selected time period
  * @param publArr {arr} (CSV generated by csv-parse)
  * @returns new array with Feature objects, containing properties with artist data
*/
function createFeatureArray(dataObj, givenFeatureArray, type, datasetDescr) {
  if (dataObj != null) {
    var lon, lat;

    for (var i = 0; i < dataObj.length; i++) {
      if (dataObj[i]['lon'] && dataObj[i]['lat']) {
        lon = dataObj[i]['lon']; //* [Num]
        lat = dataObj[i]['lat']; //* [Num]
      } else {
        //no assigned location, use empty string
        lon = '';
        lat = '';
      }
      var name = dataObj[i]['name']; //* [Str]
      var address = dataObj[i]['address']; //* [Str]

      var processedAddress;
      if (dataObj[i]['processed_address']) {
        //p. dijstelberge's dataset
        var processedAddress = dataObj[i]['processed_address'];
      }

      var startYear = dataObj[i]['start_year']; //[YYYY]
      var endYear = dataObj[i]['end_year']; //[YYYY]

      var wktStr = dataObj[i]['wkt']; //[Str]
      var wkt;
      if (wktStr) {
        //w. li's dataset
        wkt = mapsDataHelper.createGeometryObjectFromString(wktStr);
      } else {
        //do not create wkt array
      }

      var occupationYear;
      if (dataObj[i]['occupation']) {
        //p. dijstelberge's dataset, known occupations
        occupationYear = dataObj[i]['occupation'];
      }

      var category = dataObj[i]['category']; //here: occupation [Str]

      var immigrant = dataObj[i]['immigrant']; //here: immigrant [Str]

      var birthYear = dataObj[i]['birth_year']; //here: birthyear [Str]

      var ecarticoID;
      if (dataObj[i]['person_id']) {
        //w. li's dataset
        ecarticoID = dataObj[i]['person_id'];
      }

      var picartaUrl;
      if (dataObj[i]['picarta_id']) {
        //p. dijstelberge's dataset
        picartaUrl = dataObj[i]['picarta_id'];
      }

      //custom id
      var id = type + '-' + i;
      var featureObj = createFeatureObject(id, lon, lat, wkt, type, name, startYear, endYear, picartaUrl, occupationYear, address, processedAddress, category, ecarticoID, datasetDescr, immigrant, birthYear);
      //limitation for now: no proper street-only address to match with geojson
      if (featureObj) {
        givenFeatureArray.push(featureObj);
      }
    }
  }
  return true;
}

/* ** end dataset processing features ** */
/* ** ******************************* ** */


/* ************************************** */
/* ** Data / input processing helper functions ** */

/**
  * @desc get the index number of an js object property
  * @param yearRange (int)
*/
Array.prototype.indexOfObject = function (property, value) {
  for (var i = 0, len = this.length; i < len; i++) {
    if (this[i][property] === value) return i;
  }
  return -1;
}

/* ** end Data / input processing helper functions ** */
/* ************************************** */


/* ************************* */
/* ** map layer functions ** */

/**
  * @desc Removes any map layer with a certain name
  * @param layerName {string}
*/
function removeMapLayer(layerName) {
  var mapLayer = getMapLayerByClassName(layerName);
  if (mapLayer) {
    map.removeLayer(mapLayer);
  }
}

/**
  * @desc Updates (or creates) a tilelayer with a certain name (using ATM tileserver)
  * @param layerName {string}
  * @param mapName {string}
*/
function addATMTileLayer(layerName, mapName) {
  //standard maps contains .png map tiles
  var url = TILEMAP_CREATE_URL_PREFIX + mapName + TILEMAP_CREATE_URL_SUFFIX;
  //however, loman map contains .jpeg map tiles, use different suffix
  if (mapName == 'loman') {
    url = TILEMAP_CREATE_URL_PREFIX + mapName + TILEMAP_CREATE_URL_LOMAN_SUFFIX;
  }
  addTileLayer(layerName, url);
}

/**
  * @desc (generic) Adds a layer with a certain name to the map
  * @param layer {TileLayer | VectorLayer}
  * @param layerName {name}
  * @todo check order of layers after adding a new one, vector layer ends up under raster layer after adding raster layer
*/
function addLayerToMap(layer, layerName) {
  //remove the previous streetplan layer (if any) before adding the current one
  removeMapLayer(layerName);
  map.addLayer(layer);
}

/**
  * @desc (generic) Updates (or creates) a tilelayer with a certain name
  * @param layerName {string}
  * @param mapName {name }
*/
//add tilePixelRatio: 2 to XYZSource for Retina display
function addTileLayer(layerName, layerUrl) {
  var tileLayer = new TileLayer({
    className: layerName,
    opacity: 0.8,
    source: new XYZSource({
      className: layerName,
      url: layerUrl,
      crossOrigin: 'Anonymous'
    }),
  });
  addLayerToMap(tileLayer, layerName);
  //always make tile layer appear at the bottom of the layer list
  tileLayer.setZIndex(-1);
}

/**
  * @desc Updates (or creates) a tilelayer with a certain name using AdamLink data
  * @param layerName {string}
  * @param year {string}
*/
function addAdamLinkStreetsVectorLayer(layerName, year) {
  var url = GEOJSON_ADAMLINK_LOCAL_URL + year + GEOJSON_ADAMLINK_LOCAL_URL_SUFFIX;
  addVectorLayer(layerName, url)
}

/**
  * @desc (generic) Updates (or creates) a vector layer with a certain name
  * @param layerName {string}
  * @param mapName {name }
*/
function addVectorLayer(layerName, layerUrl) {
  var vectorLayer = new VectorLayer({
    className: layerName,
    source: new VectorSource({
      url: layerUrl,
      format: new GeoJSON()
    }),
    style: function (feature, resolution) {
      return mapsStyling.setVectorLayerStyle(feature, resolution, mapsStyling.streetsVectorLayerStyle, ENABLE_3D_MAP);
    },
    declutter: true,
  });
  addLayerToMap(vectorLayer, layerName);
  //always make vector layer appear in middle of  layer list
  vectorLayer.setZIndex(0);
  //clamp to ground to make it appear below points layer

  console.log('[log] 3D map enabled: ', ENABLE_3D_MAP)
  if (ENABLE_3D_MAP) {
    vectorLayer.set('altitudeMode', 'clampToGround');
  }
}

/**
  * @desc (generic) Updates (or creates) a vector data layer with a certain name
  * @param layerName {string}
  * @param mapName {name}
*/
function addVectorDataLayer(layerName, givenFeatureArr) {
  var featuresArr = mapsYearHelper.getFeatureArrayForYearRange(mapsYearHelper.getStartYearOfRange(), mapsYearHelper.getEndYearOfRange(), givenFeatureArr);
  mapsStyling.setCurrentColorSchemeProperty(mapsStyling.getCurrentColorSchemeProperty());

  //set global var with all current vector points layer data
  mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer = givenFeatureArr;
  var vectorDataLayer = new VectorLayer({
    className: layerName,
    source: new VectorSource({
      features: featuresArr
    }),
    style: function (feature) {
      return mapsStyling.styleFunctionVectorDataLayer(feature);
    }
  });
  addLayerToMap(vectorDataLayer, layerName);
  //always make vector points layer appear on top of layer list
  vectorDataLayer.setZIndex(1);
}

/**
  * @desc (generic) Updates a vector layer with a certain name, used for animation -- updates the layer already in use without modifying all properties
  * @param layerName {string}
  * @param givenFeatureArr {arr} (array with features, for all years in data)
*/
function updateVectorLayerSource(layerName, givenFeatureArr) {
  var currVectorL = getMapLayerByClassName(layerName);
  var featuresArr = mapsYearHelper.getFeatureArrayForYearRange(mapsYearHelper.getStartYearOfRange(), mapsYearHelper.getEndYearOfRange(), givenFeatureArr);


  var newSource = new VectorSource({
    features: featuresArr
  });
  currVectorL.setSource(newSource);
}

/**
  * @desc (generic) Searches in all current map layers using a classname (string)
  * @param searchName {string}
  * @returns null | layer with a certain name
*/
function getMapLayerByClassName(searchName) {
  var allLayers = map.getLayers().getArray();
  for (var i = 0; i < allLayers.length; i++) {
    if (allLayers[i].getClassName() == searchName) {
      return allLayers[i];
    }
  }
  //no layers with searchName found
  return null;
}

/**
  * @desc Show a data layer on the map based on a certain feature array 
  * @param givenFeatureArray {Array} (Array of map Features)
*/
function showDataLayer(givenFeatureArray) {
  //add a vector points layer to display the given data in
  addVectorDataLayer('vectorDataLayer', givenFeatureArray);
  //create a color scheme array (for different properties) to use 
  mapsStyling.createColorSchemeArray(mapsStyling.getCurrentColorSchemeProperty());
  //create bullets list
  var currentlyFilteredItemsArr = mapsYearHelper.getFeatureArrayForYearRange(mapsYearHelper.getStartYearOfRange(), mapsYearHelper.getEndYearOfRange(), mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
  maps2DGUI.createTextualPointsList(currentlyFilteredItemsArr);
  //reset the previous information
  maps2DGUI.resetDisplayedObjectListInformation();
  //show time slider and years button
  maps2DGUI.showTimeSlider(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
  maps2DGUI.showYearsButton();
  //add active year to "year" button, set the slider to the current year
  maps2DGUI.timeSliderLabel = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
  maps2DGUI.timeSliderCurrentYear = mapsYearHelper.currentYear;
  //finally, open the tab with the object list
  maps2DGUI.openObjectListTab();
}

/**  
 * @desc features that intersect dragbox geometry are added to the collection of selected features
 */
function selectMultipleFeaturesOnMap() {
  // if the view is not obliquely rotated the box geometry and
  // its extent are equalivalent so intersecting features can
  // be added directly to the collection
  var rotation = map.getView().getRotation();
  var oblique = rotation % (Math.PI / 2) !== 0;
  var candidateFeatures = oblique ? [] : maps2DGUI.getSelectedFeatures();
  var extent = dragBox.getGeometry().getExtent();
  //apply the selection to the currently active data layer
  var vectorSource = getMapLayerByClassName('vectorDataLayer').getSource();
  vectorSource.forEachFeatureIntersectingExtent(extent, function (feature) {
    candidateFeatures.push(feature);
  });
  // when the view is obliquely rotated the box extent will exceed its geometry, so both the box and the candidate
  // feature geometries are rotated around a common anchor to confirm that, with the box geometry aligned with its extent, the geometries intersect
  if (oblique) {
    var anchor = [0, 0];
    var geometry = dragBox.getGeometry().clone();
    geometry.rotate(-rotation, anchor);
    var extent$1 = geometry.getExtent();
    candidateFeatures.forEach(function (feature) {
      var geometry = feature.getGeometry().clone();
      geometry.rotate(-rotation, anchor);
      if (geometry.intersectsExtent(extent$1)) {
        maps2DGUI.addFeatureToSelectedFeatures(feature);
      }
    });
  }
  var numFeatures = maps2DGUI.getSelectedFeatures().getLength();
  //a selection should consist of at least one selected point, otherwise deselect
  if (numFeatures >= 1) {
    //a selection of 1+ map points has been made
    maps2DGUI.hideObjectTab();
    maps2DGUI.openObjectListTab();
    //show or hide the search box
    maps2DGUI.hideSearchBox();
    //reset currently active search filter (textual)
    maps2DGUI.resetTextualSearchFilter();
    maps2DGUI.filterObjects();
    //update item list if it is visible
    if (maps2DGUI.getItemListImagesVisibleStatus()) {
      maps2DGUI.loadItemListAsImagesData();
    }
  } else {
    maps2DGUI.resetSpatialSelections();
    //update item list if it is visible
    if (maps2DGUI.getItemListImagesVisibleStatus()) {
      maps2DGUI.loadItemListAsImagesData();
    }
  }
}

/* ** end map layer functions ** */
/* ***************************** */

/* ******************** */
/* ** Main functions ** */


/**
  * @desc Create the map element, initial map layers and initial view
  * @todo Base view on const value, via browser parameter?
*/
function createMap(defaultLon, defaultLat, defaultZoom, defaultRotation) {
  map = new Map({
    //disable default controls, but recreate the same buttons in bootstrap
    controls: [],
    //do not define any map layers yet, done in main() function
    layers: [],
    target: 'map',
    view: new View({
      //amsterdam as the center	
      center: mapsDataHelper.fromLonLat([defaultLon, defaultLat]),
      zoom: defaultZoom,
      rotation: defaultRotation
    })
  });
}

/**
  * @desc Update the current data layer
  * @param layerName {Str}
  * @param givenFeatureArr {Arr} Array of Features
  * @todo check division of function with updateDataLayer()
  */
function updateCurrentDataLayer(layerName, givenFeatureArr) {
  if (maps2DGUI.getItemListImagesVisibleStatus()) {
    maps2DGUI.setItemListImagesHtmlText('');
    //load with delay: only load data IF user has stopped at a certain year to prevent running multiple queries
    maps2DGUI.loadItemListAsImagesDataWithDelay();
  }
  if (ENABLE_3D_MAP && maps3DGUI != null && maps3DGUI.get3DEnabled()) {
    //workaround 3D map perspective: we have to add a new vectorDataLayer, since the Cesium 3D map doesn't auto-update otherwise
    addVectorDataLayer(layerName, mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
  } else {
    //for 2D map perspective: update only the source of the vector points layer
    updateVectorLayerSource(layerName, givenFeatureArr);
  }
}

/**
  * @desc Add UI interaction handlers to UI elements (click handlers)
  */
function addInteractionHandlersUI() {
  //unhide application contents (due to contents loading before CSS is initialized)
  loadingScreen.style.display = 'none';
  allApplicationContent.style.visibility = 'visible';

  //by default, hide infocard image (not in use for 2D maps at the moment)
  maps2DGUI.hideInfoCardImageContainer();

  //check for ecartico IDs, if not available: hide UI features that depend on this ID
  if (featureArrayPoints.length > 0) {
    if (featureArrayPoints[0].get('ecarticoID') == null) {
      maps2DGUI.hideShowItemListAsImagesButton();
      maps2DGUI.hideLinkedDataTab();
    }
  }

  //activate tooltips where they have been added
  maps2DGUI.activateBootstrapTooltips();

  // Update the current slider value (each time you drag the slider handle)
  maps2DGUI.timeSliderMinYear = mapsYearHelper.minYear;
  maps2DGUI.timeSliderMaxYear = mapsYearHelper.maxYear;

  //button for closing an item
  buttonCloseObject.addEventListener('click', function () { maps2DGUI.hideObjectTab(); maps2DGUI.openObjectListTab(); maps2DGUI.deselectSingleFeatureOnMap(); });

  //add behavior to "toggle sidebar" pill button (hide sidebar) [][][X]
  toggleSidebarHiddenButton.addEventListener('click', function () { maps2DGUI.toggleSideBar() });

  //add behavior to time slider
  inputTimeSlider.oninput = function () {
    mapsYearHelper.currentYear = (Number(this.value));
    maps2DGUI.timeSliderLabel = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);

    updateCurrentDataLayer('vectorDataLayer', mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
    //always show current year in tooltip
    maps2DGUI.timeSliderTooltip = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
    maps2DGUI.forceTimeSliderTooltipDisplay();
    //update items in list
    var currentlyFilteredItemsArr = mapsYearHelper.getFeatureArrayForYearRange(mapsYearHelper.getStartYearOfRange(), mapsYearHelper.getEndYearOfRange(), mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
    maps2DGUI.createTextualPointsList(currentlyFilteredItemsArr);
    maps2DGUI.searchObjectList(maps2DGUI.getCurrentSearchFilter());
    //reset selected map point
    //maps2DGUI.deselectSingleFeatureOnMap();
    maps2DGUI.resetSpatialSelections();
  }

  //hide the time slider until a dataset is loaded
  maps2DGUI.hideTimeSlider();
  maps2DGUI.timeSliderTooltip = 'Use time slider to adjust time range of selected data layer';

  //activate filterObjects when typing in input box
  inputFieldFilterInput.oninput = function () {
    maps2DGUI.filterObjects();
  }

  //click on close button alert to remove filters
  alertFilterActiveCloseButton.addEventListener('click', function () {
    maps2DGUI.hideFilterAlert();
    maps2DGUI.showSearchBox();
    maps2DGUI.resetTextualSearchFilter();
    maps2DGUI.resetSpatialSelections();
    maps2DGUI.filterObjects();
    maps2DGUI.hideFilterAlert();
    //if needed, update list of images
    if (maps2DGUI.getItemListImagesVisibleStatus()) {
      maps2DGUI.loadItemListAsImagesData();
    }
  });

  //hide images item list view, show textual list
  buttonDisplayItemListAsTextualList.addEventListener('click', function () {
    maps2DGUI.showItemListAsTextualList();
  });

  //hide images item list view, show textual list
  buttonDisplayItemListAsImages.addEventListener('click', function () {
    maps2DGUI.showItemListAsImages();
    maps2DGUI.loadItemListAsImagesData();
  });

  //Click handler for map --> update sidebar
  map.addEventListener('click', function (evt) {
    displaySidebarInfo(map, evt);
  });

  //Click behavior UI buttons
  zoomIn.addEventListener('click', function () {
    map.getView().setZoom(map.getView().getZoom() + 1);
  });
  zoomOut.addEventListener('click', function () {
    map.getView().setZoom(map.getView().getZoom() - 1);
  });

  //annotation functionality
  buttonSaveAnnotation.addEventListener('click', function (evt) {
    maps2DGUI.saveAnnotation(maps2DGUI.getCurrentAnnotation());
  });

  alertAnnotationField.addEventListener('click', function (evt) {
    maps2DGUI.hideAnnotationAlert();
    maps2DGUI.saveAnnotation(null);
    maps2DGUI.resetCurrentAnnotationTextField();
  });

  //Maps dropdown menu options
  buttonDisableTileLayer.addEventListener('click', function () {
    removeMapLayer('tilelayer');
    maps2DGUI.setMapsMenuBarFont(buttonDisableTileLayer);
  });
  buttonTileLayer1.addEventListener('click', function () {
    addATMTileLayer('tilelayer', 'berckenrode');
    maps2DGUI.setMapsMenuBarFont(buttonTileLayer1);
  });
  buttonTileLayer2.addEventListener('click', function () {
    addATMTileLayer('tilelayer', 'debroen');
    maps2DGUI.setMapsMenuBarFont(buttonTileLayer2);
  });
  buttonTileLayer3.addEventListener('click', function () {
    addATMTileLayer('tilelayer', 'loman');
    maps2DGUI.setMapsMenuBarFont(buttonTileLayer3);
  });
  buttonTileLayer4.addEventListener('click', function () {
    addATMTileLayer('tilelayer', 'pw-1909');
    maps2DGUI.setMapsMenuBarFont(buttonTileLayer4);
  });
  buttonTileLayer5.addEventListener('click', function () {
    addTileLayer('tilelayer', 'http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}');
    maps2DGUI.setMapsMenuBarFont(buttonTileLayer5);
  });
  buttonFullScreen.addEventListener('click', function () {
    maps2DGUI.toggleFullScreen();
  });

  //Streetplan layers dropdown menu options
  buttonDisableVectorLayer.addEventListener('click', function () {
    removeMapLayer('streetplan');
    maps2DGUI.setStreetplansMenuBarFont(buttonDisableVectorLayer);
  });
  buttonVectorLayer1.addEventListener('click', function () {
    addAdamLinkStreetsVectorLayer('streetplan', '1625');
    maps2DGUI.setStreetplansMenuBarFont(buttonVectorLayer1);
  });
  buttonVectorLayer2.addEventListener('click', function () {
    addAdamLinkStreetsVectorLayer('streetplan', '1700');
    maps2DGUI.setStreetplansMenuBarFont(buttonVectorLayer2);
  });
  buttonVectorLayer3.addEventListener('click', function () {
    addAdamLinkStreetsVectorLayer('streetplan', '1909');
    maps2DGUI.setStreetplansMenuBarFont(buttonVectorLayer3);
  });

  //set name of dataset button according to current dataset
  maps2DGUI.setButtonLabel(buttonDataLayer1, DATASET_LABEL + ' (as points)');
  //Data layers dropdown menu options
  buttonDataLayer1.addEventListener('click', function () {
    //always reset current search filter upon adding a new dataset
    maps2DGUI.resetAllFiltersAndSelectionsInItemList();
    showDataLayer(featureArrayPoints);
    maps2DGUI.setDataMenuBarFont(buttonDataLayer1);
  });
  //include option for polygons
  if (featureArrayPolygons.length > 0) {
window.polies = featureArrayPolygons;
    maps2DGUI.showVectorDataLayerPolygonsButton();
    maps2DGUI.setButtonLabel(buttonDataLayer2, DATASET_LABEL + ' (as vectors)');
    buttonDataLayer2.addEventListener('click', function () {
      //always reset current search filter upon adding a new dataset
      maps2DGUI.resetAllFiltersAndSelectionsInItemList();
      showDataLayer(featureArrayPolygons);
      maps2DGUI.setDataMenuBarFont(buttonDataLayer2);
    });
    if (SHOW_POLYGONS) {
      //default is set to polygons, highlight this menu option
      maps2DGUI.setDataMenuBarFont(buttonDataLayer2);
    }
  }

  buttonDisableDataLayer.addEventListener('click', function () {
    maps2DGUI.resetAllFiltersAndSelectionsInItemList();
    //hide slider and years button
    maps2DGUI.hideTimeSlider();
    maps2DGUI.hideYearsButton();
    //remove the points layer with data
    removeMapLayer('vectorDataLayer');
    //close/hide the current tabs and open the defaulttab
    maps2DGUI.showOnlyDefaultTab();
    maps2DGUI.setDataMenuBarFont(buttonDisableDataLayer);
  });

  //View range buttons in Years menu (only appear after selecting points layer)
  buttonViewAllYears.addEventListener('click', function () {
    mapsYearHelper.currentYear = 0;
    maps2DGUI.hideTimeSlider();
    maps2DGUI.timeSliderLabel = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
    //update textual list of features
    updateDataLayer();
    maps2DGUI.setYearsMenubarFont(buttonViewAllYears);
  });
  buttonViewSingleYear.addEventListener('click', function () {
    mapsYearHelper.setCurrentYearToDefaultYear();
    mapsYearHelper.yearRange = 1;
    maps2DGUI.showTimeSlider(mapsYearHelper.currentYear, mapsYearHelper.yearRange);
    maps2DGUI.timeSliderLabel = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
    updateDataLayer();
    maps2DGUI.setYearsMenubarFont(buttonViewSingleYear);
  });
  buttonView10Years.addEventListener('click', function () {
    mapsYearHelper.setCurrentYearToDefaultYear();
    mapsYearHelper.yearRange = 10;
    maps2DGUI.showTimeSlider(mapsYearHelper.currentYear, mapsYearHelper.yearRange);
    maps2DGUI.timeSliderLabel = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
    updateDataLayer();
    maps2DGUI.setYearsMenubarFont(buttonView10Years);
  });
  buttonView20Years.addEventListener('click', function () {
    mapsYearHelper.setCurrentYearToDefaultYear();
    mapsYearHelper.yearRange = 20;
    maps2DGUI.showTimeSlider(mapsYearHelper.currentYear, mapsYearHelper.yearRange);
    maps2DGUI.timeSliderLabel = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
    updateDataLayer();
    maps2DGUI.setYearsMenubarFont(buttonView20Years);
  });

  //Animation buttons in Years menu (only appear after selecting points layer)
  buttonAnimateYears.addEventListener('click', function () {
    playYearAnimation();
  });
  buttonPauseYears.addEventListener('click', function () {
    animationIsPlaying = false;
  });
  buttonResetYears.addEventListener('click', function () {
    //todo: really reset animation functions..
    animationIsPlaying = false;
    mapsYearHelper.setCurrentYearToFirstYear();
    updateCurrentDataLayer('vectorDataLayer', mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
    maps2DGUI.timeSliderLabel = maps2DGUI.getCurrentYearLabel(mapsYearHelper.currentYear, mapsYearHelper.yearRange, mapsYearHelper.minYear, mapsYearHelper.maxYear);
  });

  //Tools menu
  buttonActivate3DMode.addEventListener('click', function () {
    if (maps3DGUI != null) {
      console.log('[log] activating 3D mode');
      maps3DGUI.toggleCesium3DMode();
    }
    console.log('[err] 3D mode turned off')
  });
  buttonResetColorScheme.addEventListener('click', function () {
    console.log('[log] resetting color scheme');
    mapsStyling.setCurrentColorSchemeProperty(null);
    //always reset current search filter upon adding a new dataset
    maps2DGUI.resetTextualSearchFilter();
    showDataLayer(mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
  });
  buttonSetColorSchemeToCategory.addEventListener('click', function () {
    console.log('[log] resetting color scheme');
    mapsStyling.setCurrentColorSchemeProperty('category');
    //always reset current search filter upon adding a new dataset
    maps2DGUI.resetTextualSearchFilter();
    showDataLayer(mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
  });
  buttonSetColorSchemeToName.addEventListener('click', function () {
    console.log('[log] resetting color scheme');
    mapsStyling.setCurrentColorSchemeProperty('name');
    //always reset current search filter upon adding a new dataset
    maps2DGUI.resetTextualSearchFilter();
    showDataLayer(mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
  });
  buttonToggleColorPalette.addEventListener('click', function () {
    console.log('[log] toggling color palette');
    //always reset current search filter upon adding a new dataset
    mapsStyling.toggleColorPalette();
    maps2DGUI.resetTextualSearchFilter();
    showDataLayer(mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
  });

  //close button for controls information panel
  	//remove the window after infobox alert is clicked
	alertInfoBoxClose.addEventListener('click', function () {
		maps2DGUI.hideInfoBoxAlert();
	});

  //multiselect features
  //clear selection when drawing a new box and when clicking on the map
  dragBox.on('boxstart', function () {
    console.log('[log] Starting multi-select');
    //hide search box while dragging a box (since multiselect and search cannot be used at the same time)
    maps2DGUI.resetSpatialSelections();
    maps2DGUI.hideSearchBox();
  });
  //dragbox handler (cmd+drag, ctrl+drag)
  dragBox.on('boxend', function () {
    selectMultipleFeaturesOnMap();
  });
  //allow for selecting multiple map features
  map.addInteraction(dragBox);
  //allow for selecting map items
  map.addInteraction(select);


  // Create an overlay to anchor the tooltip to the map
  const overlay = new Overlay({
    element: tooltip,
    offset: [8, 0],
    positioning: 'bottom-left'
  });
  map.addOverlay(overlay);

  // Display the tooltip on hover
  map.on('pointermove', function(event) {
    const pixel = map.getEventPixel(event.originalEvent);
    const vectorLayer = getMapLayerByClassName('vectorDataLayer'); // Get the vector layer by class name

    const feature = map.forEachFeatureAtPixel(pixel, function(feature, layer) {
      if (layer === vectorLayer) {
        return feature;
      }
    });

    if (feature) {
      // Get the feature's style
      const style = feature.getStyle();
      var fillColor = 'rgba(80,80,80,0.9)'
      if(style.getFill() != null) {
        fillColor = style.getFill().getColor();
      }
      mapToolTipText.style.textShadow = "-1px 1px 0px "+fillColor+", 1px 1px 0px "+fillColor+", 1px -1px 0px "+fillColor+", -1px -1px 0px "+fillColor;
      const coordinates = feature.getGeometry().getCoordinates();
      overlay.setPosition(coordinates);
      var featureName = feature.get('name');
      mapToolTipText.innerHTML = featureName;
      tooltip.style.display = 'block';
    } else {
      tooltip.style.display = 'none';
    }
  });

}

/* ** End main functions ** */
/* ************************ */

  /**
    * @desc Display sidebar information, based on a click on the map
    * @param map {Obj} openlayers map
    * @param evt {Event} (click event)
    * @todo perhaps, simplify tab behavior using jquery instead of plain js
  */
  function displaySidebarInfo(map, evt) {
    const vectorLayer = getMapLayerByClassName('vectorDataLayer'); // Get the vector layer by class name

    var feature = map.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {
      if (layer === vectorLayer) {
        return feature;
      }
    });
    //if a feature is returned, show info (under mouse click x,y coordinates)
    if (feature) {
      maps2DGUI.showSideBarContentForFeature(feature);
    } else {
      maps2DGUI.deselectSingleFeatureOnMap();
    }
    var dataHelper = new MapsDataHelper();

    console.log("[log] current coordinate: " + dataHelper.toLonLat(evt.coordinate));
  }

/**
  * @desc Main application loop
*/
async function main() {
  //show "about" modal when loading application
	textGeneralAbout.innerHTML = "This 2D maps research environment demonstrator was created in the context of the <a href='https://www.virtualinteriorsproject.nl' target='_blank'>Virtual Interiors project</a> (2018-22) & extended in 2024. It can be used via a desktop or tablet browser. See also <a href='https://doi.org/10.5281/zenodo.11457262' target='_blank'>paper DH Benelux 2024</a> & <a href='https://gitlab.com/uvacreate/virtual-interiors/virtual-interiors-2d-research-envi/-/tree/main' target='_blank'>documentation</a>.<br><br><i>Data & research: Weixuan Li. App creation & user research: Hugo Huurdeman. For all contributors & data sources, see About-page</i>.";
	$('#demoIntroductionOverlay').modal('show');
  //show information about controls
  maps2DGUI.showInfoBoxAlert('introText',true);

  //read the dataset CSVs and create featureArray with the data
  var currentData;
  try {
    currentData = await inputDataHelper.parseCSV(DATASET_URL);
  } catch (err) {
    alert('Error loading CSV data ("' + DATASET_URL + '") :' + err);
  }

  //create feature array as points, default
  createFeatureArray(currentData, featureArrayPoints, 'pointData', DATASET_LABEL);

  /*
  //create feature array as wkt, if included
  if (inputDataHelper.columnInSourceData('wkt', currentData)) {
    createFeatureArray(currentData, featureArrayPolygons, true, 'polygonData', DATASET_LABEL);
  }
  */

  //create an empty map (now centered on Amsterdam)
  createMap(DEFAULT_LON, DEFAULT_LAT, DEFAULT_ZOOM, DEFAULT_ROTATION);
  //enable Cesium map
  if (ENABLE_3D_MAP) {
    maps3DGUI = new Maps3DGUI(map, maps2DGUI, ENABLE_3D_MAP);
    maps3DGUI.enable3DMap(map);
  }
  //add the default ATM tile layer to the map
  addATMTileLayer('tilelayer', 'debroen');
  //add the default streets vector layer to the map
  addAdamLinkStreetsVectorLayer('streetplan', '1700');

  //add interaction handlers to the map and UI
  addInteractionHandlersUI();

  //add default dataset to map (artists/points)
  if (SHOW_POLYGONS) {
    showDataLayer(featureArrayPolygons);
  } else {
    showDataLayer(featureArrayPoints);
  }
}

main();
