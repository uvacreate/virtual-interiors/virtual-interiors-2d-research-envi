/**
 * @classdesc MapsVectorDataLayer class data storage, and contains helper functions for the currently active vector data layer.
 */
class MapsVectorDataLayer {
  constructor() {
    //array which will point to current vector points array with all years (for animation a subset will be shown)
    this._completeFeatureArrayForCurrentVectorDataLayer = [];
  }

  /**
  * @desc get a complete feature array for the current data layer
  */
  get completeFeatureArrayForCurrentVectorDataLayer() {
    return this._completeFeatureArrayForCurrentVectorDataLayer;
  }

  /**
  * @desc set a complete feature array for the current data layer
  */
  set completeFeatureArrayForCurrentVectorDataLayer(featureArr) {
    this._completeFeatureArrayForCurrentVectorDataLayer = featureArr;
  }

  /**
  * @desc get a Feature object from a specified property (e.g. 'ecarticoID', 2259)
  * @param featureArr {arr}
  * @returns feature {Feature}
  */
  getFeatureInCurrentVectorDataLayerByProperty(property, propertyVal) {
    var featureArr = this.completeFeatureArrayForCurrentVectorDataLayer;
    for (var i = 0; i < featureArr.length; i++) {
      if (featureArr[i].get(property) == propertyVal) {
        return featureArr[i];
      }
    }
    return null;
  }

  /**
  * @desc get minimum and maximum value for a certain js object property
  * @param property {str} js object property as str
  * @returns [minVal, maxVal] {Array}
  */
  getMinMaxValueForVectorDataLayerProperty(property) {
    var maxVal = 0;
    var minVal = 0;
    for (var i = 0; i < this.completeFeatureArrayForCurrentVectorDataLayer.length; i++) {
      var currVal = parseInt(this.completeFeatureArrayForCurrentVectorDataLayer[i].get(property));
      //todo: check if number ..
      if (currVal > maxVal) {
        maxVal = currVal;
      }
      if (currVal < minVal) {
        minVal = currVal;
      }
    }
    return [minVal, maxVal];
  }

  /**
    * @desc Get the number of unique values for a certain property
    * @param property {str} js object property as str
  */
  getNumberOfUniqueValuesForProperty(property) {
    var uniqPropArr = [];
    for (var i = 0; i < this.completeFeatureArrayForCurrentVectorDataLayer.length; i++) {
      var currVal = this.completeFeatureArrayForCurrentVectorDataLayer[i].get(property);
      if (uniqPropArr.indexOf(currVal) == -1) {
        uniqPropArr.push(currVal);
      }
    }
    return uniqPropArr.length;
  }
}

export default MapsVectorDataLayer;