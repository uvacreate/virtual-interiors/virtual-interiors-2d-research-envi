//use papa-parse to more easily load/parse csv files
var PapaParse = require('papaparse');

/**
 * @classdesc InputDataHelper class contains functions related to retrieving and validating input data (CSV and URL parameters).
 */
class InputDataHelper {
    constructor() {
    }

    /**
      * @desc Checks if a column is in the source data (format returned by PapaParse), based on column name/first row
      * @return Column in source data {Bool}
    */
    columnInSourceData(colName, dataObj) {
        if (dataObj[0][colName]) {
            return true;
        } else {
            return false;
        }
    }

    /**
      * @desc Parses a CSV file using PapaParse plugin
      * @return Parsed data {arr} ([column1,column2,..][row1,row2,..])
    */
    parseCSV(url) {
        return new Promise((resolve, reject) => {
            PapaParse.parse(url, {
                download: true,
                header: true,
                complete(results, url) {
                    console.log(results.errors);
                    //console.log(results.meta);
                    //console.log(results.data);
                    resolve(results.data);
                },
                error(err, url) {
                    reject(err);
                }
            })
        })
    }

    /**  
     * @desc Converts a string to a bool. This conversion will: match 'true', 'on', or '1' as true, ignore all white-space padding, ignore capitalization (case),'  tRue  ','ON', and '1   ' will all evaluate as true.
     * @param s {Str} input string 
    */
    parseBoolean(s) {
        // will match one and only one of the string 'true','1', or 'on' regardless
        // of capitalization and regardless of surrounding white-space.
        var regex = /^\s*(true|1|on)\s*$/i
        return regex.test(s);
    }

    /**  
    * @desc Check input given as a URL parameter
    * @param paramStr {Str} parameter string to check for in the URL parameter
    * @param defaultVal {Str} default value if no parameter is found
    * @param parseTypeFunction {Func} function to check/validate the input string
    */
    checkParameter(paramStr, defaultVal, parseTypeFunction) {
        //get the specified parameter string
        var param;
        //check for parameter, catch malformedURL expecptions
        try {
            var param = this.getParameterByName(paramStr);
        } catch (err) {
            alert('Error in one of the parameters (' + err + ').');
            console.log('error');
            return defaultVal;
        }
        if (param != null) {
            //try to parse the value
            try {
                var parsed = parseTypeFunction(param);
                return parsed;
            } catch (err) {
                alert('Error in one of the parameter values (' + err + ').');
                console.log('error');
                return defaultVal;
            }
        }
        return defaultVal;
    }

    /**  
     * @desc Check an input URL specified as a URL parameter
     * @param s {Str} input string 
    */
    checkDatasetURL(urlStr) {
        if (urlStr.startsWith(`http`)) {
            //try to construct (validate) a URL, error is caught in parent function
            var url = new URL(urlStr);
        }
        if (urlStr.startsWith('https://docs.google.com/spreadsheets/')) {
            //The Google sheets url included as a URL parameter is seen as part of the URL
            //this means that the parameters in the Google sheets URL (including &) are seen as regular parameters
            //here, we add them again to the URL
            return urlStr + '&single=true&output=csv';
        } else {
            return urlStr;
        }
    }

    /**
    * @desc (generic) get input file from browser-parameter (https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript)
    * @param name {str}
    * @param url {str}
    * @todo might not work with the cesium3d plug-in
    */
    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
}

export default InputDataHelper;