import OLCesium from 'ol-cesium';

/**
 * @classdesc Maps3DGUI class manages GUI and functionality for Cesium 3D maps. Ol-cesium is a connecting layer between OpenLayers (2D mapping framework) and Cesium-3D (3D mapping framework). 
 */
class Maps3DGUI {
    constructor(map, maps2DGUI, enable3dMapBool) {
        //Variables for Cesium 3D map
        this.Cesium;
        //3D map object
        this.ol3d;
        //const to indicate if 3D map functionality is enabled
        this.ENABLE_3D_MAP = enable3dMapBool;
        //map variable (from main.js)
        this.map = map;
        //maps2DGUI (from main.js)
        this.maps2DGUI = maps2DGUI;
    }

    /**
      * @desc Get the status of whether the 3D map perspective is shown
      * @param layerName {string}
    */
    get3DEnabled() {
        if (this.ENABLE_3D_MAP) {
            //check if 3D mode is enabled
            if (this.ol3d.getEnabled()) {
                //Cesium enabled, 3D mode turned on
                return true;
            } else {
                //Cesium enabled, 3D mode turned off
                return false;
            }
        } else {
            //Cesium disabled, generic 2D mode
            return false;
        }
    }

    /**
      * @desc Toggle the Cesium 3D map view
      * @todo try to fix streetnames display issue (seems impossible at the moment)
     */
    toggleCesium3DMode() {
        if (this.ol3d) {
            if (this.ol3d.getEnabled()) {
                console.log('[log] switching to 2D mode');
                this.ol3d.setEnabled(false);
            } else {
                console.log('[log] switching to 3D mode');
                this.ol3d.setEnabled(true);
            }
        }
    }

    /**
      * @desc Set alternative interaction handlers for Cesium 3D map instance. Normally, this is not possible using Ol-cesium, and the documentation even indicates this, but using this approach it works, creating a connection point between the 2D GUI and 3D maps.
      * @param handler {Cesium.ScreenSpaceEventHandler}
      * @param ol3dScene {CesiumScene}
    */
    setCesiumInteractionHandlers(handler, ol3dScene) {
        //right now, only set click interaction handler (also possible to e.g. add mouse move handler)
        handler.setInputAction(function onLeftClick(movement) {
            //only use handler when 3D is enabled
            if (this.get3DEnabled()) {
                var pickedFeature = ol3dScene.pick(movement.position);
                if (pickedFeature) {
                    var feature = pickedFeature.primitive.olFeature;
                    if (feature) {
                        this.maps2DGUI.showSideBarContentForFeature(feature);
                    }
                }
            }
        }.bind(this), this.Cesium.ScreenSpaceEventType.LEFT_CLICK);
    }

    /**
      * @desc Enable the Cesium 3D map instance
      * @todo Check why points layer is under streets layer
    */
    enable3DMap(map) {
        //import Cesium from 'cesium/Cesium';
        this.Cesium = require('cesium/Cesium');
        window.Cesium = this.Cesium; // expose Cesium to the OL-Cesium library
        /* 
        //widgets appear not to be functioning
        require('cesium/Widgets/widgets.css');
        */
        //create new map3d object
        this.ol3d = new OLCesium({
            map: map,
        });
        //temporary fix for easy console debugging
        window.ol3d = this.ol3d;
        //turn off 3D map by default
        this.ol3d.setEnabled(false);
        this.ol3d.enableAutoRenderLoop();
        const ol3dScene = this.ol3d.getCesiumScene();
        var handler = new this.Cesium.ScreenSpaceEventHandler(ol3dScene.canvas);
        /*
        //optional: set resolution scale (2 is higher resolution, but results in smaller map markers as well)
        ol3d.setResolutionScale(2);
        */
        //set input handlers for cesium 3d map (mouse), by default unsupported
        this.setCesiumInteractionHandlers(handler, ol3dScene);
    }
}

export default Maps3DGUI;