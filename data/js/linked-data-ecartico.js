import SPARQLQuery from './sparql-query.js';
//use other LinkedData class for creating SPARQL queries including variable values
import LinkedData from './linked-data.js';

/**
 * @classdesc LinkedDataEcartico class contains linked data related functions to retrieve data from Ecartico/CREATE SPARQL endpoint, including endpoint and query strings.
 * @extends LinkedData
*/
class LinkedDataEcartico extends LinkedData {
  constructor(maps2DGUI, mapsVectorDataLayer) {
    //call superclass
    super();

    //public sparql endpoints
    this.SPARQL_ENDPOINT_CREATE = 'https://data.create.humanities.uva.nl/sparql';

    //sparql queries
    //single variable sparql queries, _VAR1_ is replaced by a specified variable using sparql-query.js helper
    this.SPARQL_Q_GET_ECARTICO_EXT_VOCABULARIES = 'PREFIX owl: <http://www.w3.org/2002/07/owl#> PREFIX schema: <http://schema.org/> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT * WHERE { GRAPH <https://data.create.humanities.uva.nl/id/ecartico/> { ?ecartico owl:sameAs ?externalRelations ; schema:birthDate ?birthDate ; schema:deathDate ?deathDate . FILTER regex(?ecartico, "^https://www.vondel.humanities.uva.nl/ecartico/persons/_VAR1_$", "i"). }}';

    //sparql query to get a list of vocabularies from ecartico
    //multi variable sparql queries, _MULTIVAR_POS_ is replaced by a list specified in _MULTIVAR_. _MULTIVAR_ itself is replaced by an array of values
    this.SPARQL_Q_GET_ECARTICO_EXT_VOCABULARIES_LIST = 'PREFIX owl: <http://www.w3.org/2002/07/owl#> PREFIX schema: <http://schema.org/> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  SELECT ?ecartico ?externalRelations WHERE { GRAPH <https://data.create.humanities.uva.nl/id/ecartico/> { ?ecartico a schema:Person ; owl:sameAs ?externalRelations . BIND(STR(?ecartico) AS ?ecarticoStr) . values ?ecarticoUris { _MULTIVAR_POS_ } FILTER(STRENDS(?ecarticoStr, ?ecarticoUris ) ) . FILTER(CONTAINS(STR(?externalRelations), "wikidata")) } }';
    this.SPARQL_Q_GET_ECARTICO_EXT_VOCABULARIES_LIST_MULTIVAR = '"_MULTIVAR_" ';

    //get 2D GUI
    this.maps2DGUI = maps2DGUI;

    this.mapsVectorDataLayer = mapsVectorDataLayer;

    //this.linkedDataWikidata = new LinkedDataWikidata(this.maps2DGUI, this.mapsVectorDataLayer, this.ecarticoWikidataLookupArray);
  }

  /**
    * @desc Load SPARQL data (related vocabularies) from CREATE endpoint b/o ecarticoID
    * @param ecarticoID {str}
    * @param resultFunction {function}
  */
  async loadExternalVocabulariesSparqlData(ecarticoID) {
    if (ecarticoID == null) { return null };
    //this query retrieves all external LOD vocabularies for a specific ecartico ID
    var sparqlQueryWithVariable = new SPARQLQuery().singleVariableQuery(this.SPARQL_Q_GET_ECARTICO_EXT_VOCABULARIES, ecarticoID);
    return this.loadSparqlData(this.SPARQL_ENDPOINT_CREATE, sparqlQueryWithVariable);
  }

  /**
    * @desc Load SPARQL data from wikidata b/o list of wikidataIDs
    * @param ecarticoIDArr {Arr}
    * @param resultFunction {function}
  */
  async loadMultipleExternalVocabulariesSparqlData(ecarticoIDArr) {
    //this query retrieves all external LOD vocabularies for a specific ecartico ID
    if (ecarticoIDArr != null) {
      //only consider uniq ids
      var ecarticoUniqIDArr = this.getUniqueEcarticoIDArray(ecarticoIDArr);
      if (ecarticoUniqIDArr != null) {
        //create sparql query. To not retrieve items with partial string matches (e.g. .../52255 and .../5225), add prefix character '/'
        var sparqlQueryWithVariable = new SPARQLQuery().arrayVariableQueryV2(this.SPARQL_Q_GET_ECARTICO_EXT_VOCABULARIES_LIST, this.SPARQL_Q_GET_ECARTICO_EXT_VOCABULARIES_LIST_MULTIVAR, ecarticoUniqIDArr, '/');
        //console.log(sparqlQueryWithVariable);
        //update UI
        this.maps2DGUI.setItemListImagesHtmlText('<i>Querying Ecartico data ...</i>');
        return this.loadSparqlData(this.SPARQL_ENDPOINT_CREATE, sparqlQueryWithVariable);
      }
      return null;
    }
    //no Ecartico ids found
    console.log('[log] no Ecartico IDs found');
    //reset images
    this.maps2DGUI.setItemListImagesHtmlText('<i>No Ecartico IDs in current dataset / selection.</i>');
  }


  /**
    * @desc Get unique ecarticoIDs (as an array) from an array of Features
    * @param featureArr {Arr}
    * @returns arr {Arr} Array with uniq features
  */
  getUniqueEcarticoIDArray(featureArr) {
    var arr = [];
    for (var i = 0; i < featureArr.length; i++) {
      var featureID = featureArr[i].get('ecarticoID');
      //only add ecartico ID if not already in array (e.g., Pieter Lastman has four rows in the dataset with the same ecarticoID)
      if (arr.indexOf(featureID) == -1) {
        arr.push(featureID);
      }
    }
    if (arr[0] != null) {
      return arr;
    } else {
      return null;
    }
  }
}

export default LinkedDataEcartico;