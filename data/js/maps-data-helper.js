import { toLonLat, fromLonLat } from 'ol/proj.js';

/**
 * @classdesc MapsDataHelper class contains functions useful to process map information (e.g. lat/lon).
 */
class MapsDataHelper {
    constructor() {
    }

    /**
     * @desc normalize a value between 0-1 (given a min and max value)
     * @param val {int}
     * @param min {int}
     * @param max {int}
    */
    normalizeValues(val, min, max) {
        return (val - min) / (max - min);
    }

    /**
     * @desc Transforms a coordinate to longitude/latitude. (OL function)
    */
    toLonLat(coordinate) {
        return toLonLat(coordinate);
    }

    /**
     * @desc Transforms a coordinate from longitude/latitude to a different projection. (OL)
    */
    fromLonLat(lonLatArr) {
        return fromLonLat(lonLatArr);
    }

    /**
     * @desc Correct a lat lon with the wrong notation (Specific to Xuan's artists dataset)
     * @param coordStr {String}
     * @returns fixedCoordStr {String}
    */
    correctLatLonNotation(coordStr) {
        var cnt = 0;
        var fixedCoordStr = coordStr.replace(/\./g, function (dot) {
            if (cnt == 0) {
                cnt++;
                return dot;
            } else {
                cnt++;
                return '';
            }
        });
        return fixedCoordStr;
    }

    /**
    * @desc Helper function to create a geometry object from the string included in the csv source
    * @param geometryStr {String} (String from CSV)
    * @returns geometry object suitable for OpenLayers
    */
    createGeometryObjectFromString(geometryStr) {
        //required format: [[[-5e6, 6e6], [-5e6, 8e6], [-3e6, 8e6], ...]]
        //POLYGON ((4.893533333330001 52.366944444444, 4.893532370275334 52.36692484101594, ...))
        var type = geometryStr.split('(')[0].trim();
        //--> 'POLYGON' | 'MULTIPOLYGON'
        var geometryCoordStr;
        var finalCoordinatesArray
        //polygon: ((lon, lat))
        //multipolygon (((lon,lat)))
        if (type == 'POLYGON') {
            geometryCoordStr = geometryStr.split('(')[2].split(')')[0];
            finalCoordinatesArray = [[]];
        } else if (type == 'MULTIPOLYGON') {
            geometryCoordStr = geometryStr.split('(')[3].split(')')[0];
            finalCoordinatesArray = [[[]]];
        }
        //--> '4.893533333330001 52.366944444444, 4.893532370275334 52.36692484101594, ...'
        var geometryArrRaw = geometryCoordStr.split(',');
        //--> [' 4.893533333330001 52.366944444444'], [' 4.893532370275334 52.36692484101594'], [...]
        for (var i = 0; i < geometryArrRaw.length; i++) {
            var geomCoordStr = geometryArrRaw[i].trim();
            //--> '4.893533333330001 52.366944444444'
            var geomCoordArr = geomCoordStr.split(' ');
            //--> ['4.893533333330001'], ['52.366944444444']
            for (var j = 0; j < geomCoordArr.length; j++) {
                geomCoordArr[j] = Number(geomCoordArr[j]);
                //--> [4.893533333330001]
            }
            if (type == 'POLYGON') {
                finalCoordinatesArray[0].push(this.fromLonLat([geomCoordArr[0], geomCoordArr[1]]));
            } else if (type == 'MULTIPOLYGON') {
                finalCoordinatesArray[0][0].push(this.fromLonLat([geomCoordArr[0], geomCoordArr[1]]));
            }
        }
        var finalGeometryObject = {
            'type': type,
            'coordinates': finalCoordinatesArray
        }
        return finalGeometryObject;
    }
}

export default MapsDataHelper;