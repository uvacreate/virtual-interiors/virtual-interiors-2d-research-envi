import SPARQLQuery from './sparql-query.js';
//use other LinkedData class for creating SPARQL queries including variable values
import LinkedData from './linked-data.js';

/**
 * @classdesc LinkedDataAdamnet class contains linked data related functions to retrieve data from Adamnet SPARQL endpoint, including endpoint and query strings.
 * @extends LinkedData  
*/
class LinkedDataAdamnet extends LinkedData {
  constructor(maps2DGUI, mapsYearHelper) {
    //call superclass
    super();

    //public sparql endpoints
    this.SPARQL_ENDPOINT_ADAMNET_ALL = 'https://api.druid.datalegend.net/datasets/AdamNet/all/services/endpoint/sparql';
    this.SPARQL_ENDPOINT_ADAMNET_HERITAGE = 'https://api.druid.datalegend.net/datasets/AdamNet/Heritage/services/Heritage/sparql';

    //sparql queries
    //single variable sparql queries, _VAR1_ is replaced by a specified variable using sparql-query.js helper
    this.SPARQL_Q_GET_ADAMNET_ARTWORK_IMGS = 'PREFIX dc: <http://purl.org/dc/elements/1.1/> PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX foaf: <http://xmlns.com/foaf/0.1/> PREFIX owl: <http://www.w3.org/2002/07/owl#> SELECT * WHERE { ?work foaf:depiction ?widgetImage . ?work dc:title ?work_title . ?work dc:description ?work_description . ?work dc:type ?type . ?work dc:creator ?creator . ?creator skos:prefLabel ?creator_label . ?creator owl:sameAs ?sameas . FILTER regex(?sameas, "^https://www.vondel.humanities.uva.nl/ecartico/persons/_VAR1_$", "i"). BIND(CONCAT(\'<img data-target="#imageOverlay" data-toggle="modal" class="clickableImg" style="height:60px; margin-top:2px;" src="\',?widgetImage,\'" data-url="\',STR(?work),\'">\') AS ?widget) }';

    //query to get straatbeelden from adamnet
    //triple variable sparql queries, _VAR1_, _VAR2_, _VAR3_ are replaced by specified variables using sparql-query.js helper
    this.SPARQL_Q_GET_ADAMNET_STRAATBEELDEN_IMGS = 'PREFIX void: <http://rdfs.org/ns/void#> PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/> PREFIX dc: <http://purl.org/dc/elements/1.1/> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX hg: <http://rdf.histograph.io/> PREFIX dct: <http://purl.org/dc/terms/> PREFIX foaf: <http://xmlns.com/foaf/0.1/>  SELECT DISTINCT ?werk ?werktitel ?image ?jaar ?widget WHERE { ?werk sem:hasBeginTimeStamp ?begin . ?werk void:inDataset ?set . ?werk dct:spatial ?street . ?werk dc:title ?werktitel . ?werk dc:type ?type . ?street a hg:Street . ?street skos:prefLabel ?straatnaam . FILTER (?straatnaam="_VAR1_"^^xsd:string) BIND(IF(COALESCE(xsd:datetime(str(?begin)), "!") != "!", year(xsd:dateTime(str(?begin))),"3"^^xsd:integer) AS ?jaar ) . ?werk dc:title ?titel . ?werk foaf:depiction ?image . BIND(CONCAT(\'<img data-target="#imageOverlay" data-toggle="modal" class="clickableImg" style="height:80px; margin-top:2px;" src="\',?image,\'" data-url="\',STR(?werk),\'">\') AS ?widget) FILTER(?jaar>=_VAR2_ && ?jaar<=_VAR3_) } GROUP BY ?werk ?set LIMIT 40';

    //get 2D GUI (todo: fix)
    this.maps2DGUI = maps2DGUI;

    this.mapsYearHelper = mapsYearHelper;
  }

  /**
    * @desc Load SPARQL data from adamnet
    * @param streetNameStr {str}
    * @param resultFunction {function}
  */
  async loadStraatBeeldenSparqlData(streetNameStr) {
    //reusable function to load data from wikidata [fixme: would probably be more efficient if it loads all properties in advance. Todo: create as node.js module instead of ES6 class.] 
    if (streetNameStr == null) { return null };
    var sparqlQueryWithVariable = new SPARQLQuery().tripleVariableQuery(this.SPARQL_Q_GET_ADAMNET_STRAATBEELDEN_IMGS, streetNameStr, this.mapsYearHelper.getStartYearOfRange(), this.mapsYearHelper.getEndYearOfRange());
    return this.loadSparqlData(this.SPARQL_ENDPOINT_ADAMNET_ALL, sparqlQueryWithVariable);
  }

  /**
    * @desc Process Adamnet (street scenes) LOD returned from sparql
    * @param event {obj}
  */
  processStraatBeeldenSparqlData(resultsArr) {
    for (var i = 0; i < resultsArr.length; i++) {
      console.log(resultsArr[i].widget.value);
      var widgetHtmlText = resultsArr[i].widget.value;
      var widgetHtmlTextFixed = this.fixAdlibLink(widgetHtmlText);
      this.maps2DGUI.populateStreetScenesGalleryField(widgetHtmlTextFixed);
    }
  }

  /**
    * @desc Quick fix for incorrect Adlib links in current linked data..
    * @param event {obj}
  */
  fixAdlibLink(str) {
    console.log(str);
    var corrStr = str.replace('http://amasp.adlibhosting.com/wwwopacx_images/', 'https://amdata.adlibhosting.com/api/');
    console.log(corrStr);
    return corrStr;
  }

  /**
    * @desc Load SPARQL data from adamnet b/o ecarticoID
    * @param ecarticoID {str}
    * @param resultFunction {function}
  */
  async loadArtworkImgsSparqlData(ecarticoID) {
    /*if (ecarticoID == null) { return null };
    var sparqlQueryWithVariable = new SPARQLQuery().singleVariableQuery(this.SPARQL_Q_GET_ADAMNET_ARTWORK_IMGS, ecarticoID);
    return this.loadSparqlData(this.SPARQL_ENDPOINT_ADAMNET_HERITAGE, sparqlQueryWithVariable);//, this.processArtworkImgsSparqlData.bind(this));*/
    return null;
  }

  /**
    * @desc Process Adamnet (artworks) LOD returned from sparql
    * @param event {obj}
  */
  processArtworkImgsSparqlData(resultsArr) {
    //var resultsArr = this.getSparqlResultsArr(event);
    if(resultsArr != null) {
      if (resultsArr.length > 0) {
        var widgetHtmlText = resultsArr[0].widget.value;
        this.maps2DGUI.populateWorksGalleryField(widgetHtmlText);
      }
    }
  }
}

export default LinkedDataAdamnet;