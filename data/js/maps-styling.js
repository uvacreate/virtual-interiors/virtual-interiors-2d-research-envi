import { Fill, Stroke, Style, Text, Circle } from 'ol/style.js';
import MapsDataHelper from './maps-data-helper';
import Palette from 'google-palette';

/**
 * @classdesc MapsStyling class manages 2D (OpenLayers) map styling
 */
class MapsStyling {
    constructor(mapsVectorDataLayer, showPolygonUncertainty) {
        this.mapsVectorDataLayer = mapsVectorDataLayer;
        //standard property from dataset to base color scheme on (use getters-setters)
        this.currentColorSchemeProperty = 'category';
        //arr to store color schemes for different values (use getters-setters)
        this._currentColorSchemeArray = [];
        //tol-rainbow provides a virtually endless number of color shades
        this.LARGE_COLOR_PALETTE_NAME = 'tol-rainbow';
        //color palettes (array to cycle through) from google-palette
        //changed to "safe" palettes (>=15 colors available)
        this.COLOR_PALETTE_NAME_ARRAY = ['mpn65', 'tol-rainbow', 'rainbow', 'tol-dv', 'tol-sq'];
        //current color palette name (use getters-setters)
        this.currentColorPaletteName = this.COLOR_PALETTE_NAME_ARRAY[0];
        //bool indicating if uncertainty should be shown in polygons
        this.showPolygonUncertainty = showPolygonUncertainty;
        /* define map styles */
        //streets layer style
        this._streetsVectorLayerStyle = new Style({
            fill: new Fill({
                color: 'rgba(153,153,153,0.8)'
            }),
            stroke: new Stroke({
                color: 'rgba(200,200,200,0.9)',//ram/p[rampIndex],
                width: 2,
                alpha: 0.3
            }),
            text: new Text({
                font: '10px "Open Sans",sans-serif',
                /*text: feature.get('preflabel'),*/
                placement: 'line',
                stroke: new Stroke({
                    color: 'rgba(34, 34, 34, 0.9)',
                    width: 3
                }),
                //XX stroke is dynamically set elsewhere (loadVectorLayer())
                fill: new Fill({
                    color: 'rgba(190, 190, 190, 0.9)'
                })
                // })
            }),
            //zIndex: 2
        });
        //vector layer which is hidden (delected by filtering objects)
        this._hiddenVectorStyle = new Style({
            image: new Circle({
                fill: new Fill({ color: 'rgba(255,255,255,0.9)' }),
                stroke: new Stroke({ color: 'rgba(55,55,55,0.7)', width: 2 }),
                radius: 5
            }),
            zIndex: -1
        });
        //vector layer which is selected, by hovering over an item in the filter panel, or by selecting an item on the map
        this._selectedVectorStyle = new Style({
            //circle
            image: new Circle({
                fill: new Fill({ color: 'black' }),
                stroke: new Stroke({ color: 'white', width: 3 }),
                radius: 9
            }),
            //vect
            stroke: new Stroke({
                color: 'rgba(255,255,255,0.7)',
                width: 3
            }),
            fill: new Fill({
                color: 'rgba(0,0,0,0.8)'
            }),
            text: new Text({
                placement: 'line',
                font: '12px Calibri,sans-serif',
                fill: new Fill({
                    color: '#fff'
                }),
                stroke: new Stroke({
                    color: 'black',
                    width: 2
                })
            }),
            zIndex: 1 //,
            //declutter:true
        });
    }

    //public getters/setters
    /**
     * @desc Get style for selected/filtered data item
    */
    get selectedVectorStyle() {
        return this._selectedVectorStyle;
    }

    /**
     * @desc Get style for hidden (unselected/unfiltered) data item
    */
    get hiddenVectorStyle() {
        return this._hiddenVectorStyle;
    }

    /**
     * @desc Get style for streetplan layer
    */
    get streetsVectorLayerStyle() {
        return this._streetsVectorLayerStyle;
    }

    /**
     * @desc Get array with color scheme info
    */
    get currentColorSchemeArray() {
        return this._currentColorSchemeArray;
    }

    /**
     * @desc Set array with color scheme info
    */
    set currentColorSchemeArray(arr) {
        return this._currentColorSchemeArray = arr;
    }

    /**
     * @desc Add an object to color scheme array
    */
    addObjectToColorSchemeArray(obj) {
        this._currentColorSchemeArray.push(obj);
    }

    /**
     * @desc Style function for standard vector layers (polygons --> artist data, points --> publisher/artist data)
     * @param layerName {string}
     * @param mapName {name}
     * @todo Make more generic (when adding more data layers)
    */
    styleFunctionVectorDataLayer(feature) {
        if (feature.getGeometry() != null) {
            var featureType = feature.getGeometry().getType();
            if (featureType == 'lineString' || featureType == 'MultiLineString') {
                //feature is street, don't do anything
            } else if (featureType == 'Polygon' || featureType == 'MultiPolygon' || featureType == 'Point') {
                //feature is polygon (artists data)
                var standardVectorStyle = new Style({
                    fill: new Fill({
                        color: this.getRGBAForFeatureBasedOnProperty(feature, this.getCurrentColorSchemeProperty())//'rgba('+Math.random()*255+','+Math.random()*255+','+Math.random()*255+',0.9)'
                    }),
                    image: new Circle({
                        fill: new Fill({ color: this.getRGBAForFeatureBasedOnProperty(feature, this.getCurrentColorSchemeProperty()) }),
                        stroke: new Stroke({ color: '#333', width: 2 }),
                        radius: 5
                    })
                })
                return standardVectorStyle;
                }
            }
        return null;
    }

    /**
     * @desc Get an RGBA string for the current property
     * @param feature {Feature}
     * @param selectedColorProperty {str} dataset property to apply color to
     * @returns rgbaStr {str} || colorStr {str}
    */
    getRGBAForFeatureBasedOnProperty(feature, selectedColorProperty) {
        if (feature == null) { return; }
        //default setting for transparency
        var a = 0.9;
        var featureType;
        if (feature.getGeometry()) {
            featureType = feature.getGeometry().getType();
        }
        var propertyToColor = feature.get(selectedColorProperty);
        var csIndex = this.currentColorSchemeArray.indexOfObject('propertyToColor', propertyToColor);

        //at the moment, calculate the uncertainty of the polygon and multipolygon feature types, change alpha based on their area size
        if (featureType == 'Polygon' || featureType == 'MultiPolygon') {
            if (this.showPolygonUncertainty) {
                a = this.calculateAlphaValueBasedOnUncertainty(feature);
            } else {
                a = 0.8;
            }
        }
        if (csIndex != -1) {
            var currColorSchemeObj = this.currentColorSchemeArray[csIndex];
            //create the rgba string to use for creating item fill
            var rgbaStr = this.hexToRgbA(currColorSchemeObj.hexColor, a);
            return rgbaStr;
        } else {
            //no propertyToColor defined, e.g. when initializing map
            console.log('[log] No defined property to base color scheme on, using standard color scheme.');
            return 'lightblue';
        }
    }

    /**
        * @desc Convert a hex value to an RGBA string
        * @param hex {str}
        * @param alpha {int}
        * @returns rgbaStr {str}
    */
    hexToRgbA(hex, alpha) {
        var c;
        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            c = hex.substring(1).split('');
            if (c.length == 3) {
                c = [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c = '0x' + c.join('');
            var rgba = 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + alpha + ')';
            return rgba;
        }
        throw new Error('[err] Bad hex specified');
    }

    /**
     * @desc Get name of current color palette
    */
    getCurrentColorPaletteName() {
        return this.currentColorPaletteName;
    }

    /**
        * @desc Set name of current color palette
        * @param name {str} Google palette name
    */
    setCurrentColorPaletteName(name) {
        this.currentColorPaletteName = name;
    }

    /**
        * @desc Get color palette array
        * @returns COLOR_PALETTE_NAME_ARRAY {Arr} Array of Google palette names
    */
    getColorPaletteNameArray() {
        return this.COLOR_PALETTE_NAME_ARRAY;
    }

    /**
        * @desc Toggle a different color palette (cycles through palette name strings and applies them)
    */
    toggleColorPalette() {
        var colPalArr = this.getColorPaletteNameArray();
        var colPalIndex = colPalArr.indexOf(this.getCurrentColorPaletteName());
        if (colPalIndex < colPalArr.length - 1) {
            colPalIndex++;
        } else {
            colPalIndex = 0;
        }
        var selectedColPal = colPalArr[colPalIndex];
        console.log('[log] Color palette switched to: ' + selectedColPal);
        this.setCurrentColorPaletteName(selectedColPal);
    }

    /**
    * @desc Set the global property to use as the basis of the map items color scheme
    * @param selectedProperty {str}
    */
    setCurrentColorSchemeProperty(selectedProperty) {
        this.currentColorSchemeProperty = selectedProperty;
    }

    /**
    * @desc Get global color scheme property
    */
    getCurrentColorSchemeProperty() {
        return this.currentColorSchemeProperty;
    }

    /**
      * @desc create a color scheme array based on a selected dataset property
      * @param yearRange (int)
    */
    createColorSchemeArray(selectedColorProperty) {
        //reset global color scheme array var
        this.currentColorSchemeArray = [];
        //check currently active dataset for properties and associate colors
        for (var i = 0; i < this.mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer.length; i++) {
            var feature = this.mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer[i];
            var propertyToColor = feature.get(selectedColorProperty);

            //(1) get number of unique values of selected property
            var numUniqVals = this.mapsVectorDataLayer.getNumberOfUniqueValuesForProperty(selectedColorProperty);
            if (numUniqVals > 15) {
                //most color palettes don't have enough shades, set to largest one
                this.setCurrentColorPaletteName(this.LARGE_COLOR_PALETTE_NAME);
            }
            var palette = new Palette(this.getCurrentColorPaletteName(), numUniqVals);

            //(2) store color settings (for each unique property value)
            if (propertyToColor != null) {
                //object to store color scheme for current property
                var currColorSchemeObj;
                //helper: use custom Array "indexOfObject" prototype
                var csIndex = this.currentColorSchemeArray.indexOfObject('propertyToColor', propertyToColor);

                if (csIndex == -1) {
                    //no array item found which contains this property, hence create
                    var currArrIndex = this.currentColorSchemeArray.length;
                    var currHexColor = '#' + palette[currArrIndex];
                    //create specific object with property value and associated color
                    currColorSchemeObj = {
                        'propertyToColor': propertyToColor,
                        'hexColor': currHexColor,
                    };
                    //store in color scheme array
                    this.addObjectToColorSchemeArray(currColorSchemeObj);
                }
            }
        }
    }

    /**
     * @desc calculate an alpha value based on the uncertainty of a feature
     * @param yearRange (int)
    */
    calculateAlphaValueBasedOnUncertainty(feature) {
        var alph = 1;
        //select "areaSize" as the uncertainty property
        var uncProp = 'areaSize';
        var valueToNormalize = feature.get(uncProp);
        //now get the [min, max] value for the selected property in the current dataset
        var minMaxArr = this.mapsVectorDataLayer.getMinMaxValueForVectorDataLayerProperty(uncProp);
        var mapsDataHelper = new MapsDataHelper();
        var normalizedValue = mapsDataHelper.normalizeValues(valueToNormalize, minMaxArr[0], minMaxArr[1]);
        //do some processing with the alpha values to make them appear nicely on the map
        if (normalizedValue != 0) {
            alph = 1 - normalizedValue * 10;
            //do not allow for very low alpha values, since map items might be invisible then
            if (alph < 0.2) {
                alph = 0.2
            };
        }
        return alph;
    }

    /**
    * @desc sets a certain vector layer style 
    * @param feature {Feature}
    * @param currStyle {Style}
    */
    setVectorLayerStyle(feature, resolution, currStyle, ENABLE_3D_MAP) {
        var featureGeometry = feature.getGeometry();
        if (featureGeometry == null) {
            return null;
        } else {
            var featureType = featureGeometry.getType();

            //do not display names of points, otherwise results in an error
            if (featureType == 'LineString' || featureType == 'MultiLineString') {
                //if(!ENABLE_3D_MAP) {//(!get3DEnabled()) {
                var ends = feature.values_.geometry.ends_;
                var featNam = feature.get('preflabel');
                if (ends && featNam) {
                    //specific data issue (Adamnet): "Slagersgang" geojson entry not working ! >1700
                    if (ends.length == 3 && featureType == 'MultiLineString' && featNam == ('Slagersgang')) {
                        //console.log('[log] GeoJSON issue with: ');
                        //console.log(feature.getGeometry());
                        //console.log(featNam);
                        //console.log(feature.values_.geometry.ends_);
                        return null;
                    }
                } else {
                    return null;
                }

                if (ENABLE_3D_MAP) {
                    //only show labels at close range due to declutter not working for 3d map
                    if (resolution < 2) {
                        var placeName = feature.get('preflabel');
                        currStyle.getText().setText(placeName);
                    } else {
                        currStyle.getText().setText('');
                    }
                } else {
                    var placeName = feature.get('preflabel');
                    currStyle.getText().setText(placeName);
                }
                //}
                return currStyle;
            } else {
                //feature is not a linestring/multilinestring .. for the moment do not show points from streets data
                return null;
            }
        }
    }
}

export default MapsStyling;