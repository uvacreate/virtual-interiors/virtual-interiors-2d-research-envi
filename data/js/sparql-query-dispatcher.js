/**
* @classdesc Dispatch a SPARQL query and retrieve its contents.
* @todo No timeout included in current implementation.
*/
class SPARQLQueryDispatcher {
	constructor(endpoint) {
		this.endpoint = endpoint;
	}

	/**
	* @desc Dispatch a SPARQL query
	*/
	query(sparqlQuery) {
		const fullUrl = this.endpoint + '?query=' + encodeURIComponent(sparqlQuery);
		const headers = { 'Accept': 'application/sparql-results+json' };
		console.log(sparqlQuery);
		return fetch(fullUrl, { mode: 'cors', method: 'POST', headers }).then(body => body.json()).catch((error) => {
			console.error('Error:', error);
		});
	}
}

export default SPARQLQueryDispatcher;