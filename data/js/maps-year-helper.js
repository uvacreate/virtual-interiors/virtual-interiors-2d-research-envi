/**
 * @classdesc MapsYearHelper class stores and retrieves information related to the years covered in the current view.
 */
class MapsYearHelper {
    constructor(minYear, maxYear, defaultYear, yearRate, yearRange) {
        this._minYear = minYear;
        this._maxYear = maxYear;
        this._currentYear = this._defaultYear = defaultYear;
        //# years per second (for animation feature)
        this._yearRate = yearRate;
        //# years to display
        this._yearRange = this._defaultYearRange = yearRange;
    }

    /**
    * @desc Get first year of current range
    */
    get minYear() {
        return this._minYear;
    }

    /**
    * @desc Get last year of current range
    */
    get maxYear() {
        return this._maxYear;
    }

    /**
    * @desc Get year span of current range
    */
    get yearSpan() {
        return this._maxYear - this._minYear;
    }

    /**
    * @desc Get current rate (years/second)
    */
    get yearRate() {
        return this._yearRate;
    }

    /**
    * @desc Get initial default year of current range
    */
    get defaultYear() {
        return this._defaultYear;
    }

    /**
    * @desc Get current year range
    */
    get yearRange() {
        return this._yearRange;
    }

    /**
    * @desc Set current year range
    */
    set yearRange(range) {
        this._yearRange = range;
    }

    /**
    * @desc Get currently active year
    */
    get currentYear() {
        return this._currentYear;
    }

    /**
    * @desc Set currently active year
    * @param cYear (int) currently active year to set
    */
    set currentYear(currentY) {
        this._currentYear = currentY;
    }

    /**
    * @desc Set current year to first year of current range
    */
    setCurrentYearToFirstYear() {
        this._currentYear = this._minYear;
    }

    /**
    * @desc Set current year to default year
    */
    setCurrentYearToDefaultYear() {
        this._currentYear = this._defaultYear;
    }

    /**
    * @desc Set current year range to default year range
    */
    setYearRangeToDefaultRange() {
        this.yearRange = this._defaultYearRange;
    }

    /**
     * @desc Get the start of a selected year range, based on the currently active year (sliding window, 'n' years before current year)
     * @param yearRange (int)
    */
    getStartYearOfRange() {
        var firstYearOfRange;
        //currentYear==0 -> show all years
        if (this.currentYear == 0) {
            return this.minYear;
        }
        if (this.yearRange > 1) {
            if (this.currentYear - (this.yearRange / 2) <= this.minYear) {
                firstYearOfRange = this.minYear;
            } else {
                firstYearOfRange = this.currentYear - this.yearRange / 2;
            }
            return firstYearOfRange;
        } else {
            return this.currentYear;
        }
    }

    /**
        * @desc Get the end of a selected year range, based on the currently active year (sliding window, 'n' years after current year)
        * @param yearRange (int)
    */
    getEndYearOfRange() {
        var lastYearOfRange;
        //currentYear==0 -> show all years
        if (this.currentYear == 0) {
            return this.maxYear;
        }
        if (this.yearRange > 1) {
            if (this.currentYear + (this.yearRange / 2) >= this.maxYear) {
                lastYearOfRange = this.maxYear;
            } else {
                lastYearOfRange = this.currentYear + (this.yearRange / 2);
            }
            //if starting before the 10 year range is possible, set end year as +10
            if (lastYearOfRange - this.minYear < this.yearRange) {
                lastYearOfRange = this.minYear + this.yearRange;
            }
            return lastYearOfRange;
        } else {
            return this.currentYear;
        }
    }

    /**
    * @desc Get an array of publisher Features, for a specified time period
    * @param startY {int} Start year of selected time period
    * @param endY {int} End year of selected time period
    * @returns new array with publishers from the selected period
    */
    getFeatureArrayForYearRange(startY, endY, featureArr) {
        var returnArr = [];
        for (var i = 0; i < featureArr.length; i++) {
            var pStartY = featureArr[i].get('startYear');
            var pEndY = featureArr[i].get('endYear');

            for (var j = startY; j <= endY; j++) {
                if (j >= pStartY && j <= pEndY) {
                    returnArr.push(featureArr[i]);
                    //we need only one match in the range, so break the for loop.
                    break;
                }
            }
        }
        return returnArr;
    }
}

export default MapsYearHelper;