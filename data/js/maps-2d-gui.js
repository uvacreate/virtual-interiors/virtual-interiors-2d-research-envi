import LinkedDataAdamnet from './linked-data-adamnet.js';
import LinkedDataEcartico from './linked-data-ecartico.js';
import LinkedDataWikidata from './linked-data-wikidata.js';
import LinkedData from './linked-data.js';
import MapsDataHelper from './maps-data-helper.js';
import AnnotationDataObject from './annotation-data-object.js';

//import jquery to more easily bind actions to e.g. buttons
var $ = require('jquery');

/**
 * @classdesc Maps2DGUI class manages GUI for 2D maps, and functions associated to the GUI (mainly Bootstrap).
 */
class Maps2DGUI {
  constructor(select, mapsVectorDataLayer, mapsStyling, mapsYearHelper, dataStorage) {
    this.mapsStyling = mapsStyling;
    //this.markerVectorLayer = null;
    this.mapsDataHelper = new MapsDataHelper();
    //sidebar tab definitions
    inputFieldFilterInput.value = '';
    this.selectedFeatures = select.getFeatures();
    //store the last highlighted feature
    this.currentlySelectedFeature;
    this.currentlySelectedFeatureOriginalStyle;
    //to check: if vectordatalayer can be loaded when needed
    this.mapsVectorDataLayer = mapsVectorDataLayer;
    this.mapsYearHelper = mapsYearHelper;

    //lookup array for ecartico <> wikidata IDs
    this.ecarticoWikidataLookupArray = [];

    this.linkedData = new LinkedData();

    this.dataStorage = dataStorage;
  }

  /**
    * @desc Get object with all selected features on the map
    * @returns array with selected features
  */
  getSelectedFeatures() {
    return this.selectedFeatures;
  }

  /**
    * @desc Add a feature to the array with currently selected features
  */
  addFeatureToSelectedFeatures(feature) {
    this.selectedFeatures.push(feature);
  }

  /**
    * @desc Toggle the sidebar in the interface
  */
  toggleSideBar() {
    if (this.sideBarIsVisible()) {
      this.hideSideBar();
    } else {
      this.showSideBar();
    }
  }

  /**
    * @desc Check if sidebar is visible
  */
  sideBarIsVisible() {
    if (colSidebar.classList.contains('d-md-block')) {
      return true;
    } else {
      return false;
    }
  }

  /**
    * @desc Show the sidebar in the interface
  */
  showSideBar() {
    colSidebar.style.display = 'block';
    colSidebar.classList.add('d-md-block');
  }

  /**
    * @desc Hide the sidebar in the interface
  */
  hideSideBar() {
    //remove breakpoint and up --> for visibility column
    colSidebar.style.display = 'none';
    colSidebar.classList.remove('d-md-block');
  }

  /**
   * @desc Show bootstrap alert panel with annotation
   * @todo revise function to support multiple annotations?
  */
  showAnnotationAlert() {
    alertAnnotationField.style.display = 'block';
  }

  /**
   * @desc Hide bootstrap alert panel with annotation
   * @todo revise function to support multiple annotations?
  */
  hideAnnotationAlert() {
    alertAnnotationField.style.display = 'none';
  }

  /* ********************** */
  /* ** Search and filtering functions ** */

  /**
    * @desc Filter the items visible on the map, simply by matching and hiding <li> items, based on the filter query. Using the feature ID, map points are filtered.
  */
  filterObjects() {

    if (this.getSelectedFeatures(this.select).getLength() >= 1) {
      this.filterObjectsByID();
    } else {
      this.filterObjectsByKeyword();
    }
  }

  /**
    * @desc Sort an HTML list
  */
  sortHTMLList(domContainer) {
    // Gather all the LI's from the container
    var contents = domContainer.querySelectorAll("li");

    // The querySelector doesn't return a traditional array
    // that we can sort, so we'll need to convert the contents 
    // to a normal array.
    var list = [];
    for (var i = 0; i < contents.length; i++) {
      list.push(contents[i]);
    }

    // Sort based on innerHTML (sorts "in place")
    list.sort(function (a, b) {
      //get last child (is item name), e.g. Dirck Bontepaert(..) :
      //<li><span style=\"color: rgba(120,28,129,0.9)\">◉</span> <a href=\"#\" class=\"boldText\" id=\"artist-26\">Dirck Bontepaert (Kunstschilder)</a></li>
      var aa = a.lastChild.innerHTML;
      var bb = b.lastChild.innerHTML;
      return aa < bb ? -1 : (aa > bb ? 1 : 0);
    });

    // We'll reverse the array because our shuffle runs backwards
    list.reverse();

    // Shuffle the order based on the order of our list array.
    for (var i = 0; i < list.length; i++) {
      domContainer.insertBefore(list[i], domContainer.firstChild);
    }
  }

  /**
    * @desc Filter the items visible on the map, by matching and hiding <li> items. Based on feature IDs stored in selectedFeatures obj (spatial selections), map points are filtered.
    * @todo Merge functionality with filter..byKeyword?
  */
  filterObjectsByID() {
    var ulLoc = document.getElementById('ulObjectListLocation');
    var selectedFeaturesArr = this.getSelectedFeatures(this.select).getArray();
    var numFoundFeaturesLoc = this.filterFunctionality(ulLoc, selectedFeaturesArr, null);
    $('#badgeObjectListLocation').html(numFoundFeaturesLoc);

    var ulNoLoc = document.getElementById('ulObjectListNoLocation');
    var numFoundFeaturesNoLoc = this.filterFunctionality(ulNoLoc, selectedFeaturesArr, null);
    $('#badgeObjectListNoLocation').html(numFoundFeaturesNoLoc);

    //update indication of filtered results
    var numFoundFeatures = numFoundFeaturesLoc + numFoundFeaturesNoLoc;
    $('#spanObjectListNumberOfResults').html(numFoundFeatures);
    //update indication of current year range
    $('#spanObjectListYearRange').html(this.getCurrentYearLabel(this.mapsYearHelper.currentYear, this.mapsYearHelper.yearRange, this.mapsYearHelper.minYear, this.mapsYearHelper.maxYear));

    //show alert 
    this.showFilterAlert(numFoundFeaturesLoc);
  }

  /**
    * @desc Filtering functionality
  */
  filterFunctionality2(ul, selectedFeaturesArr, keywordFilter) {
    if (ul) {
      var a;

      var li = ul.getElementsByTagName('li');
      var numFoundFeatures = 0;

      for (var i = 0; i < li.length; i++) {
        var featureID = li[i].getElementsByTagName("a")[0].id;
        a = li[i].getElementsByTagName("a")[0];
        //Loop through all list items, and hide those who don't match the search query 

        if (selectedFeaturesArr != null) {
          for (var j = 0; j < selectedFeaturesArr.length; j++) {
            var selectedFeatureID = selectedFeaturesArr[j].get('id');
            if (featureID == selectedFeatureID) {
              this.showFilteredFeature(featureID, li[i]);
              numFoundFeatures++;
              break;
            } else {
              //deselect all matching map points
              this.hideFilteredFeature(featureID, li[i]);
            }
          }
        } else if (keywordFilter != null) {
          var txtValue = a.textContent || a.innerText;
          if (txtValue.toUpperCase().indexOf(keywordFilter) > -1) {
            this.showFilteredFeature(featureID, li[i]);
            numFoundFeatures++;
          } else {
            //deselect all matching map points
            this.hideFilteredFeature(featureID, li[i]);
          }
        }
      }
      //always sort html list alphabetically
      this.sortHTMLList(ul);
      return numFoundFeatures;
    } else {
      return 0;
    }
  }

/**
 * @desc Filtering functionality
 */
filterFunctionality(ul, selectedFeaturesArr, keywordFilter) {
  if (ul) {
    var a;
    var li = ul.getElementsByTagName('li');
    var numFoundFeatures = 0;
    var keywordFilters;
    if (keywordFilter) {
      keywordFilters = keywordFilter.split(',').map(function(kw) {
        return kw.trim().toUpperCase();
      });
    } else {
      keywordFilters = null;
    }
    for (var i = 0; i < li.length; i++) {
      var featureID = li[i].getElementsByTagName("a")[0].id;
      a = li[i].getElementsByTagName("a")[0];

      if (selectedFeaturesArr != null) {
        for (var j = 0; j < selectedFeaturesArr.length; j++) {
          var selectedFeatureID = selectedFeaturesArr[j].get('id');
          if (featureID == selectedFeatureID) {
            this.showFilteredFeature(featureID, li[i]);
            numFoundFeatures++;
            break;
          } else {
            // deselect all matching map points
            this.hideFilteredFeature(featureID, li[i]);
          }
        }
      } else if (keywordFilters != null) {
        var txtValue = (a.textContent || a.innerText).toUpperCase();
        var keywordMatch = keywordFilters.some(kw => txtValue.indexOf(kw) > -1);

        if (keywordMatch) {
          this.showFilteredFeature(featureID, li[i]);
          numFoundFeatures++;
        } else {
          // deselect all matching map points
          this.hideFilteredFeature(featureID, li[i]);
        }
      } else if(keywordFilter != null) {
        //needs some restructuring
        var txtValue = a.textContent || a.innerText;
          if (txtValue.toUpperCase().indexOf(keywordFilter) > -1) {
            this.showFilteredFeature(featureID, li[i]);
            numFoundFeatures++;
          } else {
            //deselect all matching map points
            this.hideFilteredFeature(featureID, li[i]);
          }
          
      }
    }
    // always sort HTML list alphabetically
    this.sortHTMLList(ul);
    return numFoundFeatures;
  } else {
    return 0;
  }
}

  /**
    * @desc show a filtered feature (in the list)
  */
  showFilteredFeature(featureID, li) {
    this.showFeatureOnMap(this.mapsVectorDataLayer.getFeatureInCurrentVectorDataLayerByProperty('id', featureID));
    li.style.display = "";
  }

  /**
    * @desc Hide a filtered feature (in the list)
  */
  hideFilteredFeature(featureID, li) {
    //deselect all matching map points
    this.hideFeatureOnMap(this.mapsVectorDataLayer.getFeatureInCurrentVectorDataLayerByProperty('id', featureID));
    li.style.display = "none";
  }

  /**
    * @desc Filter the items visible on the map, by matching and hiding <li> items. Based on textual match (search box) with list items, items are displayed (hidden) on map and in textual list
    * @todo Merge functionality with filter..byID?
  */
  filterObjectsByKeyword() {
    // very simple filtering functionality
    var input = document.getElementById('inputFieldFilterInput');
    var filter = input.value.toUpperCase();

    //filter both object lists and update badges
    var ulLoc = document.getElementById('ulObjectListLocation');
    var numFoundFeaturesLoc = this.filterFunctionality(ulLoc, null, filter);
    $('#badgeObjectListLocation').html(numFoundFeaturesLoc);

    var ulNoLoc = document.getElementById('ulObjectListNoLocation');
    var numFoundFeaturesNoLoc = this.filterFunctionality(ulNoLoc, null, filter);
    $('#badgeObjectListNoLocation').html(numFoundFeaturesNoLoc);

    //update indication of filtered results
    var numFoundFeatures = numFoundFeaturesLoc + numFoundFeaturesNoLoc;
    $('#spanObjectListNumberOfResults').html(numFoundFeatures);
    //update indication of current year range
    $('#spanObjectListYearRange').html(this.getCurrentYearLabel(this.mapsYearHelper.currentYear, this.mapsYearHelper.yearRange, this.mapsYearHelper.minYear, this.mapsYearHelper.maxYear));

    if (ulLoc.getElementsByTagName('li').length == 1) {
      console.log('[warning] filtering does not change number of items, do not show alert');
    }
    //show active filter notice only if this limits the visible items 
    if (numFoundFeatures < ulLoc.getElementsByTagName('li').length) {
      this.showFilterAlert(numFoundFeaturesLoc);
    } else {
      this.hideFilterAlert();
    }
  }

  /**
    * @desc Get features which are currently filtered (using spatial selection or textual selection)
    * @param featureArr {Arr} Array with Features
    * @returns resultingFeatureArr {Arr} Array with filtered Features 
  */
  getCurrentlyFilteredFeatures(featureArr) {
    var ulLoc = document.getElementById('ulObjectListLocation');
    var ulNoLoc = document.getElementById('ulObjectListNoLocation');
    var resultingFeatureArr = [];
    if (ulLoc) {
      resultingFeatureArr = this.createCurrentlyFilteredFeaturesArray(ulLoc, featureArr, resultingFeatureArr);
      if (ulNoLoc) {
        resultingFeatureArr = this.createCurrentlyFilteredFeaturesArray(ulNoLoc, featureArr, resultingFeatureArr);
      }
      return resultingFeatureArr;
    } else {
      return [];
    }
  }

  /**
    * @desc Create array with features which have currently been filtered by the user
  */
  createCurrentlyFilteredFeaturesArray(ul, featureArr, resultingFeatureArr) {
    var liColl = ul.getElementsByTagName('li');
    for (var i = 0; i < featureArr.length; i++) {
      var featureID = featureArr[i].get('id');
      for (var j = 0; j < liColl.length; j++) {
        if (liColl[j].getElementsByTagName("a")[0].id == featureID) {
          if (liColl[j].style.display != 'none') {
            resultingFeatureArr.push(featureArr[i]);
            break;
          }
        }
      }
    }
    return resultingFeatureArr;
  }

  /**
    * @desc Search for a category of items
    * @returns searchFilter {str}
  */
  searchObjectList(filter) {
    if (filter != null) {
      this.setCurrentSearchFilter(filter);
      this.filterObjects();
    } else {
      console.log('[err] no filter specified.');
    }
  }

  /* ** End search and filtering functions ** */
  /* **************************************** */

  /**
    * @desc Create a <ul> list of (clickable) points in the scene to display in the item list tab
    * @param featureArr {arr}
    * @param interfaceItemID {DOM_element} (dom element to show results in)
    * @todo very experimental, should be reimplemented! Move to 2D-GUI.
  */
  createTextualPointsList(featureArr) {
    if (featureArr.length > 0) {
      $('#spanObjectListDataLayer').html(featureArr[0].get('datasetDescription'));
      $('#spanObjectListNumberOfResults').html(featureArr.length);
      var htmlTextLocation, htmlTextNoLocation;
      htmlTextLocation = htmlTextNoLocation = '';
      var itemsLocCnt, itemsNoLocCnt;
      itemsLocCnt = itemsNoLocCnt = 0;
      var TXT_LIST_ID_PREFIX = '';
      //htmlText += ' (' + featureArr.length + ' results)';
      //htmltext to store list
      //first iteration, loop through array to create <li>
      for (var i = 0; i < featureArr.length; i++) {
        var featureID = featureArr[i].get('id');
        var currentListItemID = featureID;
        var currentListItemLabel = featureArr[i].get('name');
        var currentListItemCategory = featureArr[i].get('category');
        var currentListItemImmigrant = featureArr[i].get('immigrant');
        var currentListItemBirthYear = featureArr[i].get('birthYear');
        var currentListItemDesc = 'descr';
        var bulletColor = this.mapsStyling.getRGBAForFeatureBasedOnProperty(featureArr[i], this.mapsStyling.getCurrentColorSchemeProperty());
        if (currentListItemDesc.length > 50) {
          currentListItemDesc = currentListItemDesc.substring(0, 45) + ' (...)';
        }
        if (featureArr[i].get('hasKnownLocation') == true) {
          htmlTextLocation += '<li style="padding-bottom:0.4vh">';
          htmlTextLocation += '<span style="color: ' + bulletColor + '">◉</span> <a href="#" class="boldText textualListStyle" id="' + TXT_LIST_ID_PREFIX + currentListItemID + '">' + currentListItemLabel + '<br>' + currentListItemCategory + ', ' + currentListItemImmigrant + ', ' + currentListItemBirthYear + '</a>';
          htmlTextLocation += '</li>';
          itemsLocCnt++;
        } else {
          htmlTextNoLocation += '<li style="padding-bottom:0.4vh">';
          htmlTextNoLocation += '<span style="color: ' + bulletColor + '">✗</span> <a href="#" class="boldText textualListStyle" id="' + TXT_LIST_ID_PREFIX + currentListItemID + '">' + currentListItemLabel + '<br>' + currentListItemCategory + '</a>';
          htmlTextNoLocation += '</li>';
          itemsNoLocCnt++;
        }
      }
      badgeObjectListLocation.innerHTML = itemsLocCnt;
      ulObjectListLocation.innerHTML = htmlTextLocation;
      badgeObjectListNoLocation.innerHTML = itemsNoLocCnt;
      ulObjectListNoLocation.innerHTML = htmlTextNoLocation;
      this.addEventListenersToFeatureArr(featureArr, TXT_LIST_ID_PREFIX);
    } else {
      ulObjectListLocation.innerHTML = '[err] No data points for chosen year.';
      ulObjectListNoLocation.innerHTML = '[err] No data points for chosen year.';
    }
  }

  /**
    * @desc Add event listeners to features in textual points list arr
  */
  addEventListenersToFeatureArr(featureArr, idPrefix) {
    //add eventlisteners to list items
    for (var i = 0; i < featureArr.length; i++) {
      this.addEventListenerToFeature(featureArr[i], idPrefix);
    }
  }

  /**
    * @desc add a single event listener to a feature in the textual points list
  */
  addEventListenerToFeature(feature, idPrefix) {
    var featureID = feature.get('id');
    var currentListItemID = document.getElementById(idPrefix + featureID);
    if (currentListItemID == null) { return };
    //add event listener for mouse over (select a single map point)
    currentListItemID.addEventListener('mouseover', function (evt) {
      //remove prefix from ID
      var id = evt.target.id.replace(idPrefix, '');
      var currFeature = this.mapsVectorDataLayer.getFeatureInCurrentVectorDataLayerByProperty('id', id);
      if (currFeature) {
        this.selectSingleFeatureOnMap(currFeature, true, false);
      }
    }.bind(this), false);
    //add mouse over listener
    currentListItemID.addEventListener('mouseout', function (evt) {
      //only handle this eventlistener if the list is visible,
      // to fix disappearing selected map point after clicking on a list item
      if ($("#" + idPrefix + featureID).is(":visible")) {
        //reset the highlighted features
        this.deselectSingleFeatureOnMap();
      }
    }.bind(this), false);
    //add click listener
    currentListItemID.addEventListener('click', function (evt, listener) {
      //fix for chrome/safari: use srcElement.id instead of evt.explicitOriginalTarget.id, which only works in firefox
      //remove prefix from id
      var id = evt.srcElement.id.replace(idPrefix, '');
      var currFeature = this.mapsVectorDataLayer.getFeatureInCurrentVectorDataLayerByProperty('id', id);
      if (currFeature) {
        this.selectSingleFeatureOnMap(currFeature, false, true);
        this.showSideBarContentForFeature(currFeature);
      }
    }.bind(this), false);
  }

  /**
    * @desc Get the currently entered search filter
    * @returns searchFilter {str}
  */
  getCurrentSearchFilter() {
    return inputFieldFilterInput.value;
  }

  /**
    * @desc Reset the currently entered search filter
  */
  resetTextualSearchFilter() {
    this.setCurrentSearchFilter('');
  }

  /**
    * @desc Reset the currently entered search filter
  */
  setCurrentSearchFilter(filter) {
    inputFieldFilterInput.value = filter;
  }

  /**
  * @desc Reset selected features (multi-select)
*/
  resetSelectedFeaturesObject() {
    this.selectedFeatures.clear();
  }

  /* ************************** */
  /* ** general UI functions ** */

  /**
   * @desc Do not display a bootstrap tab in the interface
   * @param tabId {str}
  */
  hideTab(tabId) {
    tabId.style.display = 'none';
  }

  /**
      * @desc Display a bootstrap tab in the interface
      * @param tabId {str}
  */
  displayTab(tabId) {
    tabId.style.display = 'block';
  }

  /**
      * @desc Deselect a bootstrap tab in the interface
      * @param tabId {str}
      * @todo Fixme: does not change aria status
  */
  deselectTab(tabId, tabContentId) {
    tabId.classList.remove('active');
    tabContentId.classList.remove('show')
    tabContentId.classList.remove('active');
  }

  /**
      * @desc Deselect and hide the object tab
  */
  hideObjectTab() {
    this.deselectTab(tabObject, tabObjectContent);
    this.hideTab(tabObject);
  }

  /**
      * @desc Deselect and hide the objectlist tab
  */
  hideObjectListTab() {
    this.deselectTab(tabObjectList, tabObjectListContent);
    this.hideTab(tabObjectList);
  }

  /**
      * @desc Show only the default tab (general), close the others
  */
  showOnlyDefaultTab() {
    this.hideObjectTab();
    this.hideObjectListTab();
    this.selectTab(tabHome, tabHomeContent);
  }

  /**
      * @desc Open the ObjectList tab, without closing others
  */
  openObjectListTab() {
    this.displayTab(tabObjectList);
    this.selectTab(tabObjectList, tabObjectListContent);
    this.deselectTab(tabObject, tabObjectContent);
    this.deselectTab(tabHome, tabHomeContent);
  }

  /**
      * @desc Open the Object tab, without closing others
  */
  openObjectTab() {
    this.deselectTab(tabHome, tabHomeContent);
    this.deselectTab(tabObjectList, tabObjectListContent)
    this.selectTab(tabObject, tabObjectContent);
    this.displayTab(tabObject);
    //activate the default subtab (details) upon (re)opening object tab
    this.setDefaultSubTab('#tabObjectContent');
  }

  setDefaultSubTab(domObj) {
    //bootstrap behavior to show first tab
    $(domObj).find('.nav a:first').tab('show');
  }

  /**
      * @desc Select a bootstrap tab in the interface
      * @param tabId {str}
      * @todo Fixme: does not change aria status
  */
  selectTab(tabId, tabContentId) {
    tabId.classList.add('active');
    tabContentId.classList.add('show')
    tabContentId.classList.add('active');
  }

  /**
      * @desc Show time slider
  */
  showTimeSlider(currentY, yearRange, minYear, maxYear) {
    $('.timeslidercontainer').css('display', 'block');
    spanYearDisplay.innerHTML = this.getCurrentYearLabel(currentY, yearRange, minYear, maxYear);
  }

  /**
      * @desc Hide time slider
  */
  hideTimeSlider() {
    $('.timeslidercontainer').css('display', 'none');
  }

  /**
    * @desc Set label for timeslider
  */
  set timeSliderLabel(label) {
    spanYearDisplay.innerHTML = label;
  }

  /**
    * @desc Set minimum year for timeslider
  */
  set timeSliderMinYear(minY) {
    inputTimeSlider.min = minY;
  }

  /**
    * @desc Set maximum year for timeslider
  */
  set timeSliderMaxYear(maxY) {
    inputTimeSlider.max = maxY;
  }

  /**
    * @desc Set current year for timeslider
  */
  set timeSliderCurrentYear(val) {
    inputTimeSlider.value = val;
  }

  /**
    * @desc Set tooltip (with years)
  */
  set timeSliderTooltip(toolT) {
    //dispose previous tooltip to make sure bootstrap tooltip updates itself
    $('#inputTimeSlider').tooltip('dispose').tooltip({ title: toolT });
  }

  /**
    * @desc Fix to always show tooltip for timeslider
  */
  forceTimeSliderTooltipDisplay() {
    this.forceTooltipDisplay($('#inputTimeSlider'));
  }

  /**
    * @desc Fix to always show tooltip
  */
  forceTooltipDisplay(domElt) {
    domElt.tooltip('show');
  }

  /**
      * @desc Show time slider
  */
  showYearsButton() {
    $('#buttonGroupYearDisplay').css('display', 'block');
  }

  /**
      * @desc Show time slider
  */
  hideYearsButton() {
    $('#buttonGroupYearDisplay').css('display', 'none');
  }

  /**
      * @desc Toggle whether interface is shown in fullscreen or not
  */
  toggleFullScreen() {
    //firefox implementation
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
      requestFullScreen.call(docEl);
    } else {
      cancelFullScreen.call(doc);
    }
  }

  /**
      * @desc Get the label which should be shown in the UI for the current year
      * @param layerName {string}
  */
  getCurrentYearLabel(currentY, yearRange, minYear, maxYear) {
    var labelStr = '9999';
    if (yearRange > 1 || currentY == 0) {
      labelStr = this.mapsYearHelper.getStartYearOfRange(currentY, yearRange, minYear).toFixed(0) + '-' + this.mapsYearHelper.getEndYearOfRange(currentY, yearRange, minYear, maxYear).toFixed(0);
    } else {
      labelStr = currentY.toFixed(0);
    }
    return labelStr;
  }

  /**
      * @desc Make all images clickable which have the ".clickableImg" class
      * @param event {str}
  */
  makeImagesClickable() {
    //remove previous ev handler
    $('.clickableImg').off('click');
    //hide images with 404 error (straatbeelden..)
    $('.clickableImg').on('error', function (evt) {
      console.log('[log] A .clickableImg (' + evt.currentTarget.src + ') has been hidden due to an error');
      $(this).hide();
    });
    //update overlay image source
    $('.clickableImg').on('click', function (evt) {
      //set placeholder
      imageOverlayImage.src = 'data/images/dummy-object.png';
      //reset values
      $('#imageOverlayImageCaption').html('');
      $('#imageOverlayImageCreator').html('');
      //always remove click handler, to prevent issues with multiple active handlers
      $('#imageOverlayImageCreator').off('click');
      //this.imageOverlayImage.src = 'data/images/dummy-object.png';
      //get data display from event
      var src = evt.currentTarget.src;
      var url = evt.currentTarget.dataset.url;
      var creator = evt.currentTarget.dataset.creator;
      var id = evt.currentTarget.dataset.id;
      var artworkYear = evt.currentTarget.dataset.originalTitle;
      var artworkDescr = this.createToolTipTextPaintingTitle(src);
      if (src) {
        var imgUrl = src;
        //replace wikidata url with a higher res one [todo: check if this might cause issues for adamnet and other sources]
        //this.
        imageOverlayImage.src = imgUrl.replace('100px', '600px');
        if (creator) {
          $('#imageOverlayImageCreator').html('<b>Artwork by:</b> ' + creator);  
        } else { 
          $('#imageOverlayImageCreator').html('');
        }
        if(artworkYear) {
          $('#imageOverlayImageYear').html('<b>Year: </b>' + artworkYear);
        } else {
          $('#imageOverlayImageYear').html('');
        }
        if (url) {
          $('#imageOverlayImageDescription').html('<b>Description:</b> ' + artworkDescr);
          $('#imageOverlayImageSource').html('<b>Source</b>: <a target="_blank" href="' + url + '">' + url + '</a>');
        } else {
          $('#imageOverlayImageDescription').html('<b>Description:</b> ' + artworkDescr);
          $('#imageOverlayImageSource').html('');
        }
      }
    }.bind(this))

  }

  /**
      * @desc Reset all filters and selections 
      * @todo Check if async loading of publishers dataset creates issues
  */
  resetAllFiltersAndSelectionsInItemList() {
    //reset textual search filters
    this.resetTextualSearchFilter();
    //reset spatial selection filters
    this.resetSpatialSelections();
    //reset images
    this.setItemListImagesHtmlText('');
    //hide images view, show textual list
    this.showItemListAsTextualList();
    //programmatically set active state of textual filters button (no user selection)
    $('#buttonDisplayItemListAsTextualListContainer').addClass('active');
    $('#buttonDisplayItemListAsImagesContainer').removeClass('active');
  }

  /* ** end general UI functions ** */
  /* *************************** ** */


  /* *************************** ** */
  /* ** map-related UI functions ** */


  /**
      * @desc Reset spatial selections (multi-select)
  */
  resetSpatialSelections() {
    this.resetSelectedFeaturesObject();
    this.hideFilterAlert();
    this.showSearchBox();
    this.filterObjects();
  }

  /* ** end map-related UI functions ** */
  /* ******************************* ** */


  /* ************************* **/
  /* ** sidebar UI functions * **/

  /**
      * @desc Display sidebar content itself
      * @param feature {Feature} (ol Feature)
  */
  showSideBarContentForFeature(feature) {
    //highlight currently selected feature on map
    this.selectSingleFeatureOnMap(feature);
    this.resetDisplayedObjectTabInformation();
    this.openObjectTab();
    this.showObjectTabInformationForFeature(feature);
  }

  /**
      * @desc Reset content displayed in the sidebar
  */
  resetDisplayedObjectTabInformation() {
    //reset all displayed information for an item (object)
    this.setTitleFieldHtmlText('');
    this.setDescriptionFieldHtmlText('');
    this.setCategoryFieldHtmlText('');
    this.setImmigrantFieldHtmlText('');
    this.setBirthYearFieldHtmlText('');
    this.setWorksGalleryFieldHtmlText('');
    this.setPortraitsGalleryFieldHtmlText('');
    this.setStreetScenesGalleryFieldHtmlText('');
    this.setEcarticoDataFieldHtmlText('');
    this.setExternalVocabulariesFieldHtmlText('');
  }

  /**
      * @desc Reset (bulleted) object list
  */
  resetDisplayedObjectListInformation() {
    //reset selected map point
    this.deselectSingleFeatureOnMap();
    //do a search so the map markers will be (de)highlighted if needed
    this.searchObjectList(this.getCurrentSearchFilter());
  }

  /**
      * @desc Show specific information in item tab for a feature. Currently, optimized for "artists" dataset (needs ecarticoID) or "publishers" dataset (needs picarta_id). Alternatively, just show straatbeelden
      * @param feature {Feature} (ol Feature)
      * @todo Simplify and restructure function
  */
  async showObjectTabInformationForFeature(feature) {
    //add the current feature ID to the "Annotation" overlay for later access
    var id = feature.get('id');
    //DOM elt
    dataAnnotation.dataset.id = id;
    this.showAnnotationAlertForID(id);
    this.resetCurrentAnnotationTextField();
    //type is a publisher map point --> show only that publisher
    var ecarticoID = feature.get('ecarticoID');
    var picartaUrl = feature.get('picartaUrl');
    if (ecarticoID != null) {
      //"artists" dataset
      this.populateHighlightAllLocationsField(feature);
      this.setTitleFieldHtmlText(this.getFormattedTitle(feature));
      this.setDescriptionFieldHtmlText(this.getFormattedArtistDescription(feature));

      if (ecarticoID) {
        var linkedDataAdamnet = new LinkedDataAdamnet(this, this.mapsYearHelper);
        var adamnetResultObj = await linkedDataAdamnet.loadArtworkImgsSparqlData(ecarticoID).catch(error => console.error(error));
        linkedDataAdamnet.processArtworkImgsSparqlData(adamnetResultObj);

        var linkedDataEcartico = new LinkedDataEcartico(this, this.mapsVectorDataLayer, this.mapsYearHelper);
        var ecarticoResultObj = await linkedDataEcartico.loadExternalVocabulariesSparqlData(ecarticoID).catch(error => console.error(error));
        this.processExternalVocabulariesSparqlDataEcartico(ecarticoResultObj);
        if (ecarticoResultObj) {
          //populate external vocabularies field in sidebar
          this.populateExternalVocabulariesField(ecarticoResultObj);
          //populate ecartico field in sidebar
          this.populateEcarticoFields(ecarticoResultObj[0]);
        }
      }
      this.populateCategoryField(feature);
      this.populateImmigrantField(feature);
      this.populateBirthYearField(feature);
    } else if (picartaUrl != null) {
      //"publishers" dataset
      this.populateHighlightAllLocationsField(feature);

      this.setTitleFieldHtmlText(this.getFormattedTitle(feature));
      this.setDescriptionFieldHtmlText(this.getFormattedPublisherDescription(feature));
      this.populateCategoryField(feature);
    } else {
      //type is a streetname (from geojson streets layer) --> show a list of publishers for that street
      var streetName = feature.get('preflabel')
      this.setTitleFieldHtmlText(streetName);
      this.populateHighlightAllLocationsField(feature);

      /* 
      //show which publishers are associated with a street
      //needs publisher info array .. 
      setDescriptionFieldHtmlText(this.getFormattedListOfPublishersFromStreetName(publisherFeatureArray, streetName)); 
      */
      //load associated image
      var linkedDataAdamnet = new LinkedDataAdamnet(this, this.mapsYearHelper);
      var adamnetResultObj = await linkedDataAdamnet.loadStraatBeeldenSparqlData(feature.get('preflabel')).catch(error => console.error(error));
      if (adamnetResultObj != null) {
        linkedDataAdamnet.processStraatBeeldenSparqlData(adamnetResultObj);
      }
    }
  }

  /**
    * @desc Get current annotation in input field
  */
  getCurrentAnnotation() {
    return inputFieldAnnotationText.value;
  }

  /**
    * @desc Reset annotation in input field
  */
  resetCurrentAnnotationTextField() {
    inputFieldAnnotationText.value = '';
  }

  /**
    * @desc Save an annotation to the linked data storage
    * @todo revise function
  */
  saveAnnotation(text) {
    //get current ID from DOM elt
    var id = dataAnnotation.dataset.id;
    var annObj = new AnnotationDataObject(this.dataStorage, id);
    //for security purposes, convert user input to String
    annObj.userAnnotation_text = text;
    this.showAnnotationAlertForID(id);
  }

  /**
   * @desc Show a specific annotation (based on ID)
   * @param id {Str} ID of currently selected object 
   * @todo revise function
  */
  showAnnotationAlertForID(id) {
    var annotationObj = new AnnotationDataObject(this.dataStorage, id);
    var annotationText = annotationObj.userAnnotation_text;
    if (annotationText != null) {
      alertAnnotationField.innerHTML = annotationText + ' &nbsp<i class="fas fa-window-close"></i>';
      this.showAnnotationAlert();
    } else {
      this.hideAnnotationAlert();
    }
  }

  /**
    * @desc Hide linked data tab (e.g. when no linked data in dataset)
  */
  hideLinkedDataTab() {
    //hide LD accordeon element
    inspectorDataExplorationTab.style.display = 'none';
  }

  /**
    * @desc Hide linked data images feature (since no linked data is available)
  */
  hideShowItemListAsImagesButton() {
    //hide itemlist "images" button, since this depends on the EcarticoID
    buttonDisplayItemListAsImages.style.display = 'none';
  }

  /**
      * @desc Set the title field in item tab
      * @param htmlStr {str}
  */
  setTitleFieldHtmlText(htmlStr) {
    $('#infoCardTitle').html(htmlStr);
  }

  /**
      * @desc Set the category field in item tab
      * @param htmlStr {str}
  */
  setCategoryFieldHtmlText(htmlStr) {
    $('#infoCardProperty1').html(htmlStr);
  }
  /**
      * @desc Set the category field in item tab
      * @param htmlStr {str}
  */
  setImmigrantFieldHtmlText(htmlStr) {
    $('#infoCardPropertyImmigrant').html(htmlStr);
  }
  /**
      * @desc Set the category field in item tab
      * @param htmlStr {str}
  */
  setBirthYearFieldHtmlText(htmlStr) {
    $('#infoCardPropertyBirthYear').html(htmlStr);
  }

  /**
      * @desc Set the gallery field in item tab
      * @param htmlStr {str}
  */
  setWorksGalleryFieldHtmlText(htmlStr) {
    this.toggleDomElementBasedOnHtmlContent($('#infoCardProperty4'), htmlStr);
  }

  /**
      * @desc Get the gallery field in item tab
      * @todo Simplify and restructure function
  */
  getWorksGalleryFieldHtmlText() {
    return $('#infoCardProperty4').html();
  }

  /**
      * @desc Setter for item list as images
  */
  setItemListImagesHtmlText(text) {
    $('#objectListAsImages').html(text);
  }

  /**
      * @desc Getter for item list as images
  */
  getItemListImagesHtmlText() {
    return $('#objectListAsImages').html();
  }

  /**
      * @desc Getter for visibility status item list images
  */
  getItemListImagesVisibleStatus() {
    var visibleStatus = $('#objectListAsImages').css('display');
    if (visibleStatus == 'none') {
      return false;
    } else {
      return true;
    }
  }

  /**
      * @desc helper function, show or hide dom element and associated text
      * @param htmlStr {str}
      * @todo Simplify and restructure function
  */
  toggleDomElementBasedOnHtmlContent(domElement, htmlStr) {
    if (htmlStr == '') {
      domElement.html('');
      domElement.css('display', 'none');
    } else {
      domElement.css('display', 'block');
      domElement.html(htmlStr);
    }
  }

  /**
      * @desc Set the portraits gallery field in item tab
      * @param htmlStr {str}
  */
  setPortraitsGalleryFieldHtmlText(htmlStr) {
    this.toggleDomElementBasedOnHtmlContent($('#infoCardProperty5'), htmlStr);
  }

  /**
      * @desc Get the portraits gallery field in item tab
  */
  getPortraitsGalleryFieldHtmlText() {
    return $('#infoCardProperty5').html();
  }

  /**
      * @desc Set the street scenes field in item tab
      * @param htmlStr {str}
  */
  setStreetScenesGalleryFieldHtmlText(htmlStr) {
    this.toggleDomElementBasedOnHtmlContent($('#infoCardProperty7'), htmlStr);
  }

  /**
      * @desc Set the street scenes field in item tab
  */
  getStreetScenesGalleryFieldHtmlText() {
    return $('#infoCardProperty7').html();
  }


  /**
      * @desc Set the description field in item tab
      * @param htmlStr {str}
      * @todo Simplify and restructure function
  */
  setDescriptionFieldHtmlText(htmlStr) {
    this.toggleDomElementBasedOnHtmlContent($('#infoCardProperty2'), htmlStr);
    //create (i) button here with data info + mouseover?
  }

  /**
      * @desc Set the description field in item tab
      * @param htmlStr {str}
      * @todo Simplify and restructure function
  */
  setEcarticoDataFieldHtmlText(htmlStr) {
    this.toggleDomElementBasedOnHtmlContent($('#infoCardProperty3'), htmlStr);
  }

  /**
      * @desc Set the description field in item tab
      * @param htmlStr {str}
      * @todo Simplify and restructure function
  */
  setExternalVocabulariesFieldHtmlText(htmlStr) {
    this.toggleDomElementBasedOnHtmlContent($('#infoCardProperty6'), htmlStr);
  }

  /**
      * @desc Populate the category field in item tab. Add button to filter by category.
      * @param htmlStr {str}
      * @todo Simplify and restructure function
  */
  populateCategoryField(feature) {

    var featureID = feature.get('id');
    var featureCat = feature.get('category');
    this.setCategoryFieldHtmlText('<b>Category:</b> ' + feature.get('category') + ' (<a href="#" id="searchCat-' + featureID + '">filter</a>)');

    //add event listener to filter button
    var filterButtonCat = document.getElementById('searchCat-' + featureID);
    if (filterButtonCat) {
      filterButtonCat.addEventListener('click', function (evt) { this.searchObjectList(featureCat); this.openObjectListTab(); }.bind(this));
    }
  }

  /**
      * @desc Populate the immigrant field in item tab. Add button to filter by immigrant.
      * @param htmlStr {str}
      * @todo Simplify and restructure function
  */
  populateImmigrantField(feature) {

    var featureID = feature.get('id');
    var featureIm = feature.get('immigrant');
    this.setImmigrantFieldHtmlText('<b>Immigrant:</b> ' + feature.get('immigrant') + ' (<a href="#" id="searchIm-' + featureID + '">filter</a>)');

    //add event listener to filter button
    var filterButtonCat = document.getElementById('searchIm-' + featureID);
    if (filterButtonCat) {
      filterButtonCat.addEventListener('click', function (evt) { this.searchObjectList(featureIm); this.openObjectListTab(); }.bind(this));
    }
  }

  /**
      * @desc Populate the birthyear field in item tab. Add button to filter by birthyear.
      * @param htmlStr {str}
      * @todo Simplify and restructure function
  */
  populateBirthYearField(feature) {

    var featureID = feature.get('id');
    var featureBi = feature.get('birthYear');
    this.setBirthYearFieldHtmlText('<b>Birth Year:</b> ' + feature.get('birthYear') + ' (<a href="#" id="searchBi-' + featureID + '">filter</a>)');

    //add event listener to filter button
    var filterButtonCat = document.getElementById('searchBi-' + featureID);
    if (filterButtonCat) {
      filterButtonCat.addEventListener('click', function (evt) { this.searchObjectList(featureBi); this.openObjectListTab(); }.bind(this));
    }
  }

  /**
      * @desc Populate the highlight all locations field in item tab. Add button to filter by name.
      * @param htmlStr {str}
      * @todo Simplify and restructure function
  */
  populateHighlightAllLocationsField(feature) {
    var featureName = feature.get('name');
    var selectAllLocations = document.getElementById('linkSelectAllLocationsOfName');
    if (featureName != null) {
      this.showSelectAllLocationsButton();
      selectAllLocations.addEventListener('click', function (evt) { 
        this.deselectSingleFeatureOnMap();
        this.resetSpatialSelections();
        this.searchObjectList(featureName); 
        this.openObjectListTab();
        
        //quick fix -- if images are shown, make sure to switch back to the list-based view
        if(this.getItemListImagesVisibleStatus()) {
          $('#buttonDisplayItemListAsImages').removeClass('active');
          $('#buttonDisplayItemListAsImages').removeClass('show');
          $('#buttonDisplayItemListAsTextualList').addClass('active');
          $('#buttonDisplayItemListAsTextualList').addClass('show');
          $('#itemListImageContent').removeClass('active');
          $('#itemListImageContent').removeClass('show');
          $('#itemListTextContent').addClass('active');
          $('#itemListTextContent').addClass('show');
          this.showItemListAsTextualList();
        }
      }.bind(this));
    } else {
      this.hideSelectAllLocationsButton();
    }
  }

  hideSelectAllLocationsButton() {
    var selectAllLocations = document.getElementById('linkSelectAllLocationsOfName');
    selectAllLocations.style.display = 'none';
  }

  showSelectAllLocationsButton() {
    var selectAllLocations = document.getElementById('linkSelectAllLocationsOfName');
    selectAllLocations.style.display = 'block';
  }

  /**
      * @desc Hide infocard image container
  */
  hideInfoCardImageContainer() {
    $('.card-img').css('display', 'none');
  }

  /**
      * @desc Show infocard image container
  */
  showInfoCardImageContainer() {
    $('.card-img').css('display', 'block');
  }

  /**
      * @desc Show item list as textual list
  */
  showItemListAsTextualList() {
    $('#objectListContainer').css('display', 'block');
    $('#objectListAsImages').css('display', 'none');
  }

  /**
      * @desc Show item list as images
  */
  showItemListAsImages() {
    $('#objectListContainer').css('display', 'none');
    $('#objectListAsImages').css('display', 'flex');
  }

  /**
      * @desc Show search box in item list
  */
  showSearchBox() {
    var input = document.getElementById('inputFieldFilter');
    input.style.display = '';
  }

  /**
      * @desc Hide search box in item list
  */
  hideSearchBox() {
    var input = document.getElementById('inputFieldFilter');
    input.style.display = 'none';
  }

  /**
      * @desc Hide filter alert in item list
  */
  hideFilterAlert() {
    $('#alertFilterActive').css('display', 'none');
  }

  /**
      * @desc Show filter alert in item list
      * @todo Bugfix: at times, the filter alert is displayed when no textual filter is active
  */
  showFilterAlert(numFeatures) {
    if (!numFeatures) {
      numFeatures = 0;
    }
    if (this.selectedFeatures.getLength() == 0) {
      var currentSearchFilterText = this.getCurrentSearchFilter();
      if(currentSearchFilterText.length > 18) {
        currentSearchFilterText = currentSearchFilterText.substring(0,14) + " ...";
      }
      $('#alertFilterActiveText').html('Filtered: "' + currentSearchFilterText + '" ');
      $('#alertFilterActiveBadge').html(numFeatures);
    } else {
      $('#alertFilterActiveText').html('Active selection: ');
      $('#alertFilterActiveBadge').html(numFeatures);
      //$('#alertFilterActive').html('Active selection: '+ this.selectedFeatures.getLength() +' map points ('+numFeatures+' visible) <u>remove</u>');
    }
    $('#alertFilterActive').css('display', 'flex');
    this.activateBootstrapTooltips();
  }

  /* ***************************** **/
  /* ** end sidebar UI functions * **/

  /* Font related functions (menu bar) */

  /**
    * @desc Set bold font for a dom element
  */
  setBoldFont(domElt) {
    $(domElt).css('font-weight', '600');
  }

  /**
    * @desc Set medium font for a dom element
  */
  setMediumFont(domElt) {
    $(domElt).css('font-weight', '400');
  }

  /**
    * @desc Set font for "maps" menu bar item
  */
  setMapsMenuBarFont(domElt) {
    this.resetMapSelectionFont();
    this.setBoldFont(domElt);
  }

  /**
    * @desc Reset font for "maps" menu bar item
  */
  resetMapSelectionFont() {
    this.setMediumFont(buttonDisableTileLayer);
    this.setMediumFont(buttonTileLayer1);
    this.setMediumFont(buttonTileLayer2);
    this.setMediumFont(buttonTileLayer3);
    this.setMediumFont(buttonTileLayer4);
    this.setMediumFont(buttonTileLayer5);
  }

  /**
    * @desc Set font for "streetplans" menu bar item
  */
  setStreetplansMenuBarFont(domElt) {
    this.resetStreetplansSelectionFont();
    this.setBoldFont(domElt);
  }

  /**
    * @desc Reset fonts for "streetplans" menu bar item
  */
  resetStreetplansSelectionFont() {
    this.setMediumFont(buttonDisableVectorLayer);
    this.setMediumFont(buttonVectorLayer1);
    this.setMediumFont(buttonVectorLayer2);
    this.setMediumFont(buttonVectorLayer3);
  }

  /**
    * @desc Set font for "data" menu bar item
  */
  setDataMenuBarFont(domElt) {
    this.resetDataSelectionFont();
    this.setBoldFont(domElt);
  }

  /**
    * @desc Reset font for "data" menu bar item
  */
  resetDataSelectionFont() {
    this.setMediumFont(buttonDisableDataLayer);
    this.setMediumFont(buttonDataLayer1);
    this.setMediumFont(buttonDataLayer2);
  }

  /**
    * @desc Set font for "years" menu bar item
  */
  setYearsMenubarFont(domElt) {
    this.resetYearSelectionFont();
    this.setBoldFont(domElt);
  }

  /**
    * @desc Set font for "years" menu bar item
  */
  resetYearSelectionFont() {
    this.setMediumFont(buttonViewSingleYear);
    this.setMediumFont(buttonView10Years);
    this.setMediumFont(buttonView20Years);
    this.setMediumFont(buttonViewAllYears);
  }

  /**
    * @desc activate bootstrap tooltips via jQuery (after e.g. after updating them in code)
  */
  activateBootstrapTooltips() {
    $('[data-tooltip="tooltip"]').tooltip()
  }

  /**
    * @desc Show optional button to switch to polygons in "Data" menu
  */
  showVectorDataLayerPolygonsButton(text) {
    buttonDataLayer2.style.display = 'block';
  }

  /**
    * @desc Dynamically set a label for a UI item
  */
  setButtonLabel(domObj, text) {
    domObj.innerHTML = text;
  }

  /**
    * @desc Select a single feature on the map (behavior: selecting one point deselects another)
    * @param feature {Feature}, onOver {Boolean} : if mouseover (optional)
    * @todo think about shift-selecting points in a similar vein to doing the dragBox interaction (selecting a square of points)
  */
  selectSingleFeatureOnMap(newlySelectedFeature, onMouseOver, onClick) {
      //only do anything if the newly selected feature is different than the previous one.
      if (newlySelectedFeature !== this.currentlySelectedFeature || onClick) {
        //change the style of the currently selected feature to its original style
        if (this.currentlySelectedFeature) {
          this.setFeatureGeometryAsPoint(this.currentlySelectedFeature);
          this.currentlySelectedFeature.setStyle(this.currentlySelectedFeatureOriginalStyle);
        }
        //save the style of the newly selected feature
        this.currentlySelectedFeatureOriginalStyle = newlySelectedFeature.getStyle();

        //show surrounding geometry (incl. uncertainty) of point (if available). Only show when clicking an item
        if(!onMouseOver==true) { 
          this.setFeatureGeometryAsWKT(newlySelectedFeature);
        }
        //set the "selected" vector style to the newly selected feature
        newlySelectedFeature.setStyle(this.mapsStyling.selectedVectorStyle);
        
        //set the currently selected feature var to the newly selected one
        this.currentlySelectedFeature = newlySelectedFeature;
      
    }
  }

  deselectSingleFeatureOnMap() {
      //reset feature selections
      if (this.currentlySelectedFeature && this.currentlySelectedFeatureOriginalStyle) {
        this.setFeatureGeometryAsPoint(this.currentlySelectedFeature);
        this.currentlySelectedFeature.setStyle(this.currentlySelectedFeatureOriginalStyle);
      }
  }

  /**
    * @desc Hide a feature on the map, by setting a "hidden" vector style
    * @param feature {Feature}
  */
  hideFeatureOnMap(feature) {
    feature.setStyle(this.mapsStyling.hiddenVectorStyle);
  }

  /**
    * @desc Show a feature on the map, by applying a standard style to it (using style function)
    * @param feature {Feature}
  */
  showFeatureOnMap(feature) {
    var newStyle = this.mapsStyling.styleFunctionVectorDataLayer(feature);
    feature.setStyle(newStyle);
  }

  /**
    * @desc Get a formatted HTML <li> with publisher title information
    * @param feature {Feature} (OpenLayers Feature object)
    * @todo move to separate UI fields
  */
  getFormattedTitle(feature) {
    return feature.get('name');
  }

  /**
    * @desc Set feature geometry to point
    * @param feature {Feature} (OpenLayers Feature object)
    * @todo first check if geometry available
  */
  setFeatureGeometryAsPoint(feature) {
    //todo: first check if point geometry is available
    feature.setGeometryName('geometryPoint');
  }

  /**
    * @desc Set feature geometry to WKT (polygon)
    * @param feature {Feature} (OpenLayers Feature object)
    * @todo first check if geometry available
  */
  setFeatureGeometryAsWKT(feature) {
    //todo: first check if WKT geometry is available
    feature.setGeometryName('geometryWKT');
  }

  /**
    * @desc Get a formatted HTML <li> with a list of publishers, for a streetname (currently unused)
    * @param streetName {String}
    * @returns <li> of publishers
    * @todo check filtering for year range!
  */
  getFormattedListOfPublishersFromStreetName(publisherFeatureArray, streetName) {
    //currentyear, publishersArray is global var for the moment
    if (publisherFeatureArray != null) {
      var str = "<ul id='subjectsUl'>";
      for (var i = 0; i < publisherFeatureArray.length; i++) {
        if (streetName.toLowerCase() == publisherFeatureArray[i].get('processedAddress').toLowerCase()) {
          if (this.mapsYearHelper.currentYear >= publisherFeatureArray[i].get('startYear') && this.mapsYearHelper.currentYear <= publisherFeatureArray[i].get('endYear')) {
            str += '<li><a target="_blank" href="' + publisherFeatureArray[i].get('picartaUrl') + '">' + publisherFeatureArray[i].get('name') + '</a><br> (' + publisherFeatureArray[i].get('occupationYear') + '<br><i>' + publisherFeatureArray[i].get('address') + '</i>.';
          }
        }
      }
    }
    return str;
  }

  /**
    * @desc Get a formatted HTML <li> with publisher description information
    * @param feature {Feature} (OpenLayers Feature object)
    * @todo move to separate UI fields
  */
  getFormattedPublisherDescription(feature) {
    return '<b>Years active:</b> ' + feature.get('startYear') + '-' + feature.get('endYear') + '<br><b>Known occupations:</b> ' + feature.get('occupationYear') + '<br><b>Address:</b> ' + feature.get('address') + '<br><br><i class="fas fa-link"></i> <a target="_blank" href="' + feature.get('picartaUrl') + '">View Picarta entry</a>';
  }

  /* LOD-related UI functions */

  /**
    * @desc Get a formatted HTML <li> with various artist information
    * @param feature {Feature} (OpenLayers Feature object)
    * @todo move to separate UI fields
  */
  getFormattedArtistDescription(feature) {
    var ecarticoID = feature.get('ecarticoID');
    var ecarticoStr = this.getFormattedLinkForLOD('http://www.vondel.humanities.uva.nl/ecartico/persons/', ecarticoID, 'Ecartico ID')
    return ecarticoStr + '<br><b>Address:</b> ' + feature.get('address') + '<br><b>Active at this address:</b> ' + feature.get('startYear') + '-' + feature.get('endYear');
  }

  /**
    * @desc Get a formatted HTML string with LOD information
    * @param urlPrefix
    * @param id
    * @param idDescr
    * @todo move to separate UI fields
  */
  getFormattedLinkForLOD(urlPrefix, id, idDescr) {
    if (id) {
      return '<b>' + idDescr + ':</b> ' + '<a target="_blank" href="' + urlPrefix + id + '">' + id + '</a>';
    } else {
      return '<i>No ' + idDescr + ' found.</i>';
    }
  }

  /**
  * @desc Add itemlist
  * @param id {str}
  * @param artistName {str}
  * @param image {str}  
  * @param name {str}
  * @returns str {str}
  */
  getWikidataImgThumbnailHtmlText(id, artistName, image, item, creationDate) {
    var paintingTitle = this.createToolTipTextPaintingTitle(image);
    var paintingYear = this.createToolTipTextPaintingYear(creationDate);
    return '<div><img data-target="#imageOverlay"  title="' + paintingYear + '" data-description = "'+ paintingTitle +'" data-creator="' + artistName + '" data-id="' + id + '" data-placement="top" data-toggle="modal" data-tooltip="tooltip" class="clickableImg imgListItem" src="' + image + '?width=100px" data-url="' + item + '"></div>';
  }

  createToolTipTextPaintingYear(creationDate) {
    var creationYear = "";
    if(creationDate!=null) {
      const date = new Date(creationDate);
      creationYear = date.getFullYear();
    }
    return creationYear;
  }

  createToolTipTextPaintingTitle(image) {
    //get creation year instead of full string
    var paintingTitle = "";
    //quick fix: get painting name from URL
    var paintingFileNameArr = image.split("Special:FilePath/");
    if(paintingFileNameArr.length>0) {
      //get painting name (part before eg .jpg)
      var paintingNameArr = paintingFileNameArr[1].split(".");
      if(paintingNameArr.length>0) {
        paintingTitle = paintingNameArr[0];
      }
    }
    //remove URL encoding
    paintingTitle = decodeURIComponent(paintingTitle);
    return paintingTitle;
  }

  /**
    * @desc Populate the street scenes gallery field in the sidebar
    * @param widgetHtmlText {obj}
    * @todo make a generic function for populateXX()
  */
  populateStreetScenesGalleryField(widgetHtmlText) {
    if (this.getStreetScenesGalleryFieldHtmlText() == '') {
      this.setStreetScenesGalleryFieldHtmlText('<b>Street scenes ' + this.mapsYearHelper.getStartYearOfRange() + '-' + this.mapsYearHelper.getEndYearOfRange() + '</b> (source: Adamnet)<br>');
    }
    this.setStreetScenesGalleryFieldHtmlText(this.getStreetScenesGalleryFieldHtmlText() + ' ' + widgetHtmlText);
    this.makeImagesClickable();
  }

  /**
    * @desc Populate the works gallery field in the sidebar
    * @param widgetHtmlText {obj}
    * @todo make a generic function for populateXX()
  */
  populateWorksGalleryField(widgetHtmlText) {
    if (this.getWorksGalleryFieldHtmlText() == '') {
      this.setWorksGalleryFieldHtmlText('<b>Works by this artist:</b> <span class="fas fa-info-circle info-button" data-html="true" data-tooltip="tooltip" title="" data-toggle="tooltip" data-original-title="Data source: https://wikidata.org"></span><br>');
    }
    //keep additional images are already loaded from other sources (if applicable)
    this.setWorksGalleryFieldHtmlText(this.getWorksGalleryFieldHtmlText() + ' ' + widgetHtmlText);
    this.makeImagesClickable();
    this.activateBootstrapTooltips();
  }

  /**
  * @desc Populate the portraits gallery field in the sidebar
  * @param widgetHtmlText {obj}
  * @todo make a generic function for populateXX()
*/
  populatePortraitsGalleryField(widgetHtmlText) {
    if (this.getPortraitsGalleryFieldHtmlText() == '') {
      this.setPortraitsGalleryFieldHtmlText('<b>Depictions of this artist:</b> <span class="fas fa-info-circle info-button" data-html="true" data-tooltip="tooltip" title="" data-toggle="tooltip" data-original-title="Data source: https://wikidata.org"></span><br>');
    }
    this.setPortraitsGalleryFieldHtmlText(this.getPortraitsGalleryFieldHtmlText() + ' ' + widgetHtmlText);
    this.makeImagesClickable();
    this.activateBootstrapTooltips();
  }

  /**
    * @desc Populate the ecartico metadata fields (b/o birth/death info from Wikidata)
    * @param widgetHtmlText {obj}
    * @todo make a generic function for populateXX()
  */
  populateEcarticoFields(lodObj) {
    if (lodObj == null) {
      return '<i>no Ecartico information found</i>'
    } else {
      var outputStr = '';
      if (lodObj.birthDate.type == "typed-literal") {
        outputStr += '<b>Birth date:</b> <span class="fas fa-info-circle info-button" data-html="true" data-tooltip="tooltip" title="" data-toggle="tooltip" data-original-title="Data source: https://ecartico.org"></span>  ';
        outputStr += lodObj.birthDate.value + ' </br>';
      } else {
        //outputStr += 'unknown</br>';
      }
      if (lodObj.deathDate.type == 'typed-literal') {
        outputStr += '<b>Death date:</b> <span class="fas fa-info-circle info-button" data-html="true" data-tooltip="tooltip" title="" data-toggle="tooltip" data-original-title="Data source: https://ecartico.org"></span>  ';
        outputStr += lodObj.deathDate.value + ' </br>';
      } else {
        //outputStr += 'unknown</br>';
      }
    }
    this.setEcarticoDataFieldHtmlText(outputStr);
  }

  /**
    * @desc Populate the external vocabularies field in the sidebar (using vocabularies returned by ecartico)
    * @param widgetHtmlText {obj}
    * @todo make a generic function for populateXX()
  */
  populateExternalVocabulariesField(lodArr) {
    if (lodArr.length == 0) { return; }
    var str = '<b>External vocabularies:</b> <span class="fas fa-info-circle info-button" data-html="true" data-tooltip="tooltip" title="" data-toggle="tooltip" data-original-title="Data source: https://ecartico.org"></span><br>';
    for (var i = 0; i < lodArr.length; i++) {
      var extRelURL = new URL(lodArr[i]['externalRelations'].value);
      if(extRelURL.hostname!="") {
        str += '<i class="fas fa-link"></i> <a href="' + extRelURL + '" target="_blank">' + extRelURL.hostname + '</a><br>';
      } else {
        //leave out Rijksmuseum urn: for the moment
        /*str += '<i class="fas fa-link"></i>'+ extRelURL.pathname + '</a><br>';*/
      }
    }
    this.setExternalVocabulariesFieldHtmlText(str);
    this.activateBootstrapTooltips();
  }

  /**
    * @desc Add tooltips and click handlers to all elements in DOM to which it applies (custom "data-tooltip" property, "clickableImg" class)
    * @param resultFunction {function}
  */
  addTooltipsAndClickHandlersToImageList() {
    this.activateBootstrapTooltips();
    //make all images clickable
    this.makeImagesClickable();
  }

  /**
    * @desc Load data for displaying item list as images
    * @todo move scrollTop ..
  */
  async loadItemListAsImagesData() {
    //when loading data, always scroll to top to avoid confusion about image loading
    $('#tabObjectListContent').scrollTop(0);
    //first, get all features in current year range
    var featuresInCurrentYearRangeArr = this.mapsYearHelper.getFeatureArrayForYearRange(this.mapsYearHelper.getStartYearOfRange(), this.mapsYearHelper.getEndYearOfRange(), this.mapsVectorDataLayer.completeFeatureArrayForCurrentVectorDataLayer);
    //second, get the features which have currently been filtered (via item list or spatial selections)
    var currentlyFilteredFeaturesArr = this.getCurrentlyFilteredFeatures(featuresInCurrentYearRangeArr);
    //load multiple sparql items for displaying item list as images
    //reset images
    //maps2DGUI.setItemListImagesHtmlText('<i>No Ecartico IDs in current dataset / selection.</i>');
    var linkedDataEcartico = new LinkedDataEcartico(this, this.mapsVectorDataLayer);
    //var adamnetResultObj = await linkedDataAdamnet.loadArtworkImgsSparqlData(ecarticoID).catch(error => console.error(error));
    var ecarticoResultObj = await linkedDataEcartico.loadMultipleExternalVocabulariesSparqlData(currentlyFilteredFeaturesArr).catch(error => console.error(error));
    this.processMultipleExternalVocabulariesSparqlDataEcartico(ecarticoResultObj);
  }


  /*********** LOD processing for UI functions ********/

  /**
      * @desc Process SPARQL data from wikidata. Note: uses time-out to prevent 429 errors from Wikidata
      * @param wikidataID {str}
      * @param resultFunction {function}
      * @todo expects ordered data (now using ORDER BY in SPARQL query)
    */
  processMultipleArtworkImgsSparqlDataWikidata(resultsArr) {
    if(resultsArr == null) {
      return;
    }
    console.log(resultsArr);

    var REQUEST_TIMEOUT = 20;
    var MAX_IMAGES_PER_ARTIST = 10;
    var MAX_IMAGES_PER_ARTIST_IF_FEW_RESULTS = 20;

    var shownImagesObj = {};

    //prefix to discern id
    const IMG_LIST_ID_PREFIX = 'img-';
    //array of features to later add event listeners to
    var featureArrForEventListeners = [];

    for (let i = 0; i < resultsArr.length; i++) {
      var ecarticoID = this.linkedData.lookupValueInLookupArray(this.ecarticoWikidataLookupArray, this.linkedData.getIDFromLODUrl(resultsArr[i].creator.value));
      var currFeature = this.mapsVectorDataLayer.getFeatureInCurrentVectorDataLayerByProperty('ecarticoID', ecarticoID);
      //[todo] misc-creator is e.g. a painting "attributed to XX"
      var artistName = 'misc-creator';
      var id = 0;
      if (currFeature) {
        artistName = currFeature.get('name');
        id = currFeature.get('id');
      }
      if (shownImagesObj[artistName] != null) {
        shownImagesObj[artistName]++;
      } else if (artistName != 'misc-creator') {
        shownImagesObj[artistName] = 1;
      }

      var maxNumImages = MAX_IMAGES_PER_ARTIST;
      //increase max number of images when few artists have been found
      /*if(Object.keys(shownImagesObj).length<7) {
        maxNumImages = MAX_IMAGES_PER_ARTIST_IF_FEW_RESULTS;
      }*/
      if (shownImagesObj[artistName] <= maxNumImages && artistName != 'misc-creator') {
        //https://stackoverflow.com/questions/1190642/how-can-i-pass-a-parameter-to-a-settimeout-callback
        //ES6 : add a delay of 10ms to each iteration of the for-loop, to avoid flooding wikidata with requests
        setTimeout(this.displayImageGalleryForArtist(shownImagesObj[artistName], currFeature, id, artistName, resultsArr[i].image.value, resultsArr[i].item.value, resultsArr[i].creationDate.value, IMG_LIST_ID_PREFIX, featureArrForEventListeners)
          , i * REQUEST_TIMEOUT);
      }
    }

    this.addEventListenersToFeatureArr(featureArrForEventListeners, IMG_LIST_ID_PREFIX);
    //debug: show artists / #images found
    console.log('[log] Number of images found per artist:');
    console.log(shownImagesObj);
    console.log(featureArrForEventListeners);

    this.addTooltipsAndClickHandlersToImageList();
  }

  //displayed after timeout
  displayImageGalleryForArtist(sequenceNumber, currFeature, id, artistName, image, item, creationDate, imgListPrefix, featureArrForEventListeners) {
    //print artist's name above images
    if (sequenceNumber == 1) {
      var bulletColor = this.mapsStyling.getRGBAForFeatureBasedOnProperty(currFeature, this.mapsStyling.getCurrentColorSchemeProperty());
      var htmlText = '<p class="imageGalleryArtistName"><span style="color: ' + bulletColor + '">◉</span> <a href="#" class="boldText" id="' + imgListPrefix + id + '">' + artistName + '</a><p>';
      this.setItemListImagesHtmlText(this.getItemListImagesHtmlText() + htmlText);
      featureArrForEventListeners.push(currFeature);
    }
    this.setItemListImagesHtmlText(this.getItemListImagesHtmlText() + this.getWikidataImgThumbnailHtmlText(id, artistName, image, item, creationDate))
  }

  /**
      * @desc Process Wikidata (artist portraits) LOD returned from sparql
      * @param event {obj}
    */
  processArtistPortraitsSparqlDataWikidata(resultsArr) {
    if (resultsArr.length > 0) {
      if (resultsArr[0].widget) {
        var widgetHtmlText = resultsArr[0].widget.value;
        this.populatePortraitsGalleryField(widgetHtmlText);
      }
    }
  }

  /**
    * @desc Process Wikidata LOD returned from sparql
    * @param event {obj}
  */
  processArtworkImgsSparqlDataWikidata(resultsArr) {
    //var resultsArr = this.linkedData.getSparqlResultsArr(event);
    for (var i = 0; i < resultsArr.length; i++) {
      var widgetHtmlText = resultsArr[i].widget.value;
      this.populateWorksGalleryField(widgetHtmlText);
    }
  }

  /**
      * @desc Process SPARQL data from ecartico, get Wikidata IDs, do a wikidata query for found IDs
      * @param ecarticoIDArr {Arr}
      * @param resultFunction {function}
    */
  //process returned data to extract wikidata IDs (subseq: do another query)
  async processMultipleExternalVocabulariesSparqlDataEcartico(resultsArr) {
    var wikidataIDArr = [];
    for (var i = 0; i < resultsArr.length; i++) {
      var wikidataUrl = resultsArr[i].externalRelations.value;
      //use wikidataURL as stored in ecartico, e.g. "http://www.wikidata.org/entity/Q2342403" --> we are interested in the QID
      var wikidataID = this.linkedData.getIDFromLODUrl(wikidataUrl);
      if (wikidataID) {
        wikidataIDArr.push(wikidataID);
      }
      var ecarticoUrl = resultsArr[i].ecartico.value;
      var ecarticoID = this.linkedData.getIDFromLODUrl(ecarticoUrl);

      if (wikidataID && ecarticoID) {
        this.linkedData.addItemToLookupArray(this.ecarticoWikidataLookupArray, ecarticoID, wikidataID);
      }
    }
    //only do wikidata query if wikidataIDs found
    if (wikidataIDArr.length > 0) {
      var linkedDataWikidata = new LinkedDataWikidata();
      //loading msg
      this.setItemListImagesHtmlText('<i>Querying Wikidata for paintings ...</i>');
      //get artworks wikidata
      var wikidataArtworksResultObj = await linkedDataWikidata.loadMultipleArtworkImgsSparqlData(wikidataIDArr, this.mapsYearHelper.getStartYearOfRange(), this.mapsYearHelper.getEndYearOfRange()).catch(error => console.error(error));
      //reset loading msg
      this.setItemListImagesHtmlText('');
      this.processMultipleArtworkImgsSparqlDataWikidata(wikidataArtworksResultObj);
    } else {
      console.log('[log] no Wikidata IDs found');
      //reset images
      this.setItemListImagesHtmlText('<i>No paintings found on Wikidata.</i>');
    }
  }

  /**
    * @desc Process Ecartico LOD returned from sparql
    * @param event {obj}
  */
  async processExternalVocabulariesSparqlDataEcartico(resultsArr) {
    //var resultsArr = this.linkedData.getSparqlResultsArr(event);
    if (resultsArr.length > 0) {
      var wikidataUrl = this.linkedData.getWikidataUrlFromEcarticoLOD(resultsArr);
      //use wikidataURL as stored in ecartico, e.g. "http://www.wikidata.org/entity/Q2342403" --> we are interested in the QID
      var wikidataID = this.linkedData.getIDFromLODUrl(wikidataUrl);
      if (wikidataID) {
        var linkedDataWikidata = new LinkedDataWikidata();
        //load portraits of the artist (i.e. depictions) from wikidata
        var wikidataPortraitsResultObj = await linkedDataWikidata.loadArtistPortraitsSparqlData(wikidataID).catch(error => console.error(error));
        this.processArtistPortraitsSparqlDataWikidata(wikidataPortraitsResultObj);
        //use wikidata derived ID to do a wikidata sparql query to retrieve thumbs for this artist
        var wikidataArtworksResultObj = await linkedDataWikidata.loadArtworkImgsSparqlData(wikidataID).catch(error => console.error(error));
        this.processArtworkImgsSparqlDataWikidata(wikidataArtworksResultObj);
      }
    }
  }

  /**
    * @desc Load item list as images, with a delay of 'n' ms (to avoid overloading server)
  */
  loadItemListAsImagesDataWithDelay() {
    var year = this.mapsYearHelper.currentYear;
    setTimeout(function () {
      //your code to be executed after xx ms
      var yearAfterTimeout = this.mapsYearHelper.currentYear;
      if (year == yearAfterTimeout) {
        this.loadItemListAsImagesData();
      }
    }.bind(this), 1500);
  }

  /**
     * @desc Show analysis layer legend info
     * @param type {Str} 'introText' | 'object' | 'location' | 'customizeMesh'. Otherwise: hide infobox
    */
  showInfoBoxAlert(type, showCloseButton) {
    //make this infobox visible
    alertInfoBox.style.display = 'block';
    alertInfoBoxClose.style.display = 'none';
    //optionally show close button
    if (showCloseButton) {
        alertInfoBoxClose.style.display = 'block';
    }
    //depending on type, show different kinds of information
    if (type == 'introText') {
        alertInfoBoxTitle.innerHTML = 'Info: map viewer controls <i class="far fa-question-circle"></i>';
        alertInfoBoxDescription.innerHTML = '<i class="fas fa-mouse-pointer"></i>&nbsp;&nbsp; Use <b>mouse</b> to select map items<br> <i class="fas fa-search"></i>&nbsp;&nbsp;Use <b>item list</b> to search (separate multiple strings by comma) <br> <i class="fas fa-vector-square"></i>&nbsp;&nbsp;<b>ctrl (Windows) / cmd (Mac) + drag mouse</b> to select an area<br></i> <i class="fas fa-grip-lines"></i>&nbsp;&nbsp;use <b>time-slider</b> to adjust time range</i>';
        //analysis layer: object certainty
    /*} else if (type == 'object') {
        alertInfoBoxTitle.innerHTML = 'Confidence index: objects';
        alertInfoBoxDescription.innerHTML = '<span class="uncertaintyOne">&#9673</span> 1. modeled after original object<br><span class="uncertaintyTwo">&#9673</span> 2. primary source + analogy with high certainty degree<br><span class="uncertaintyThree">&#9673</span> 3. primary source + analogy with doubt<br><span class="uncertaintyFour">&#9673</span> 4. uncertain';
        //analysis layer: location certainty
    } else if (type == 'location') {
        alertInfoBoxTitle.innerHTML = 'Confidence index: object locations';
        alertInfoBoxDescription.innerHTML = '<span class="uncertaintyOne">&#9673</span> 1. known/in original location<br><span class="uncertaintyTwo">&#9673</span> 2. inferred with certainty<br><span class="uncertaintyThree">&#9673</span> 3. inferred with doubt<br><span class="uncertaintyFour">&#9673</span> 4. uncertain';
        //customize mesh
    } else if (type == 'customizeMesh') {
        alertInfoBoxTitle.innerHTML = 'Object customization';
        alertInfoBoxDescription.innerHTML = '<span class="far fa-square alertInfoBoxIcon"></span> Press <b>q</b> for bounding box<br><span class="fas fa-arrows-alt alertInfoBoxIcon"></span> Press <b>w</b> to move object<br><span class="fas fa-sync-alt alertInfoBoxIcon"></span> Press <b>e</b> to rotate object<br><span class="fas fa-arrows-alt-v alertInfoBoxIcon"></span> Press <b>r</b> to scale object<br><span class="bg-color-five"><span class="fas fa-exclamation-triangle alertInfoBoxIcon"></span> Experimental: reload app to undo changes</span>'
    */
    } else {
        //hide infobox
        alertInfoBox.style.display = 'none';
        console.log('[log] analysis layer type unknown');
    }
  }
  
      /**
     * @desc Hide analysis layer legend info
    */
      hideInfoBoxAlert() {
        alertInfoBox.style.display = 'none';
    }
}



export default Maps2DGUI;
