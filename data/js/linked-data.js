//use custom class for dispatching SPARQL queries
import SPARQLQueryDispatcher from './sparql-query-dispatcher.js';

/**
 * @classdesc LinkedData class contains linked data related functions, shared by subclasses (..-wikidata.js, ..-ecartico.js, ..-adamnet.js)
 */
class LinkedData {
  constructor() {
  }

  /**
   * @desc (generic) Load SPARQL data from endpoint
   * @param endPointUrl {str}
   * @param sparqlQuery {str}
   * @param resultFunction {function}
  */
  async loadSparqlData(endPointUrl, sparqlQuery) {
    if (endPointUrl == null || sparqlQuery == null) { console.log("[err] No endpoint URL or SPARQL query provided"); return null; };
    const queryDispatcher = new SPARQLQueryDispatcher(endPointUrl);
    var returnValue = queryDispatcher.query(sparqlQuery).then((sparqlResultObj) => {
      //convert returned object (including result.. bindings..) to an array with the found results
      if (sparqlResultObj) {
        return this.getSparqlResultsArr(sparqlResultObj);
      } else {
        return null;
      }
    });
    return returnValue;
  }

  /**
    * @desc (generic) Get results from SPARQL data event
    * @param endPointUrl {str}
    * @param sparqlQuery {str}
    * @param resultFunction {function}
  */
  getSparqlResultsArr(event) {
    if (event) {
      if (event.results) {
        if (event.results.bindings) {
          var resultsArr = event.results.bindings;
          if (resultsArr.length > 0) {
            return resultsArr;
          }
        }
      }
    }
    //to avoid errors, always return empty array
    return [];
  }

  /**
   * @desc (generic) get an id from an LOD URI by getting the last part of the URI
   * @param url {Str} LOD Uri
   * @return {Str} processed ID 
   */
  getIDFromLODUrl(url) {
    if (url) {
      var spl = url.split('/');
      var id = spl[spl.length - 1];
      return id;
    }
    return null;
  }

  /**
   * @desc get a WikidataURL from the LOD returned by Ecartico
   * @param widgetHtmlText {obj}
   * @todo make a generic function for populateXX()
 */
  getWikidataUrlFromEcarticoLOD(lodArr) {
    for (var i = 0; i < lodArr.length; i++) {
      var extRelVal = lodArr[i].externalRelations.value;
      if (extRelVal.includes('wikidata')) {
        return extRelVal;
      }
    }
    return null;
  }

  /**
  * @desc Add corresponding values (value1<>value2) to a lookup array (generic), e.g. ecarticoID<>wikidataID. Purpose is to improve loading times and avoid issuing same SPARQL queries multiple times.
  * @param arr {Arr} Arr to store values in
  * @param idOne {Str}
  * @param idOne {Str}
  * @todo [opt] create functions which skip a certain ecartico ID if it is already stored (== less SPARQL queries)
  */
  addItemToLookupArray(arr, idOne, idTwo) {
    var idObj = [idOne, idTwo];
    if (this.getIndexOfK(arr, idOne) == -1) {
      arr.push(idObj);
    } else {
      //console.log('[log] id1: ' + idOne + ', id2: ' + idTwo + ' has already been added to the lookup table');
    }
  }

  /**
   * Generic function: index of Multidimensional Array
   * @param arr {Array} - the input array
   * @param k {object} - the value to search
   * @return {Array} 
   */
  getIndexOfK(arr, k) {
    for (var i = 0; i < arr.length; i++) {
      var index = arr[i].indexOf(k);
      if (index > -1) {
        return [i, index];
      }
    }
    return -1
  }

  /**
    * @desc (generic) Lookup corresponding value (value1<>value2) in lookup array (generic), e.g. ecarticoID<>wikidataID
    * @param arr {Arr} Arr to store values in
    * @param val {Str}
    * @todo [opt] create functions which skip a certain ecartico ID if it is already stored (== less SPARQL queries)
    * @todo [limitation/potential bug] might return the wrong value (val1/val2) if the same value exists in the two columns
  */
  lookupValueInLookupArray(arr, val) {
    for (var i = 0; i < arr.length; i++) {
      if (arr[i][0] == val) {
        return arr[i][1];
      } else if (arr[i][1] == val) {
        return arr[i][0];
      }
    }
  }
}

export default LinkedData;