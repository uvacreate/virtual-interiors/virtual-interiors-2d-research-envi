import SPARQLQuery from './sparql-query.js';
//use other LinkedData class for creating SPARQL queries including variable values
import LinkedData from './linked-data.js';

/**
 * @classdesc LinkedDataWikidata class contains linked data related functions to retrieve data from Wikidata SPARQL endpoint, including endpoint and query strings.
 * @extends LinkedData 
*/

class LinkedDataWikidata extends LinkedData {
  constructor(maps2DGUI, mapsVectorDataLayer, ecarticoWikidataLookupArray) {
    //call superclass
    super();

    //public sparql endpoints
    this.SPARQL_ENDPOINT_WIKIDATA = 'https://query.wikidata.org/sparql';

    //sparql queries
    //single variable sparql queries, _VAR1_ is replaced by a specified variable using sparql-query.js helper
    this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS = 'SELECT ?item ?itemLabel ?creator ?creatorLabel ?image ?widget WHERE { ?item wdt:P31/wdt:P279* wd:Q838948 . ?item wdt:P170 ?creator . SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". } ?item wdt:P170 wd:_VAR1_ ; wdt:P18 ?image . BIND(CONCAT(\'<img data-target="#imageOverlay" data-toggle="modal" class="clickableImg" style="height:60px; margin-top:2px;" src="\',STR(?image),\'?width=100px" data-url="\',STR(?item),\'">\') AS ?widget) . } LIMIT 20';
    this.SPARQL_Q_GET_WIKIDATA_ARTIST_PORTRAITS = 'SELECT ?image ?wikicommonsgallery ?widget WHERE { ?item wdt:P31 wd:Q5.  BIND(wd:_VAR1_ AS ?item).  OPTIONAL { ?item wdt:P18 ?image. ?item wdt:P935 ?wikicommonsgallery } BIND(CONCAT(\'<img data-target="#imageOverlay" data-toggle="modal" class="clickableImg" style="height:120px; margin-top:2px;" src="\',STR(?image),\'?width=200px" data-url="\',STR(?item),\'">\') AS ?widget) . }';
    //test
    this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_V2 = 'SELECT ?item ?creator ?image ?creationDate ?widget WHERE { VALUES ?creatorFilter { wd:_VAR1_ } ?item wdt:P170 ?creatorFilter, ?creator; wdt:P571 ?creationDate; wdt:P18 ?image . FILTER(?creator = ?creatorFilter) . BIND(CONCAT(\'<img data-target="#imageOverlay" data-toggle="modal" class="clickableImg" style="height:60px; margin-top:2px;" src="\', STR(?image), \'?width=100px" data-url="\', STR(?item), \'">\') AS ?widget) . } ORDER BY (?creator) LIMIT 20';
    this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_YEAR_FILTER = 'SELECT ?item ?creator ?image ?creationDate ?widget WHERE { VALUES ?creatorFilter { wd:_VAR1_ } ?item wdt:P170 ?creatorFilter, ?creator; wdt:P571 ?creationDate; wdt:P18 ?image. FILTER(YEAR(?creationDate) >= _VAR2_ && YEAR(?creationDate) <= _VAR3_) . BIND(CONCAT(\'<img data-target="#imageOverlay" data-toggle="modal" class="clickableImg" style="height:60px; margin-top:2px;" src="\', STR(?image), \'?width=100px" data-url="\', STR(?item), \'">\') AS ?widget) . } ORDER BY (?creator) LIMIT 500';

    //sparql query to get a list of artwork images from wikidata
    //multi variable sparql queries, _MULTIVAR_POS_ is replaced by a list specified in _MULTIVAR_. _MULTIVAR_ itself is replaced by an array of values
    this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_LIST = 'SELECT ?item ?creator ?image WHERE {  VALUES ?creatorFilter { _MULTIVAR_POS_ } . ?item wdt:P170 ?creatorFilter . ?item wdt:P170 ?creator . ?item wdt:P18 ?image . } ORDER BY ?creator LIMIT 500';
    this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_LIST_V2 = 'SELECT ?item ?creator ?image ?creationDate WHERE {  VALUES ?creatorFilter { _MULTIVAR_POS_ } ?item wdt:P170 ?creatorFilter, ?creator; wdt:P571 ?creationDate; wdt:P18 ?image . FILTER(YEAR(?creationDate) >= _VAR1_ && YEAR(?creationDate) <= _VAR2_ && ?creator = ?creatorFilter)} ORDER BY ?creator LIMIT 500';
    this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_LIST_MULTIVAR = 'wd:_MULTIVAR_';

    //get 2D GUI (todo: fix)
    this.maps2DGUI = maps2DGUI;

    this.mapsVectorDataLayer = mapsVectorDataLayer;

    this.ecarticoWikidataLookupArray = ecarticoWikidataLookupArray;
  }

  /**
    * @desc Load SPARQL data from wikidata b/o wikidataID
    * @param wikidataID {str}
    * @param resultFunction {function}
  */
  loadArtworkImgsSparqlData(wikidataID) {
    if (wikidataID == null) { return null };
    var sparqlQueryWithOneVariable = new SPARQLQuery().singleVariableQuery(this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_V2, wikidataID);
    //var sparqlQueryWithThreeVariables = new SPARQLQuery().tripleVariableQuery(this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_YEAR_FILTER, wikidataID, startYear, endYear);
    return this.loadSparqlData(this.SPARQL_ENDPOINT_WIKIDATA, sparqlQueryWithOneVariable);
  }

  /**
    * @desc Load SPARQL data from wikidata b/o list of wikidataIDs
    * @param wikidataIDArr {Arr}
    * @param resultFunction {function}
  */
  loadMultipleArtworkImgsSparqlData(wikidataIDArr, startYear, endYear) {
    if (wikidataIDArr == null) { return null };
    //1. replace _VAR1_ and _VAR2_ with the years that we will look at
    var sparqlQueryWithStartAndEndYear = new SPARQLQuery().doubleVariableQuery(this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_LIST_V2, startYear, endYear);
    //2. replace _MULTIVAR_ with the array of wikidata IDs
    var sparqlQueryWithVariable = new SPARQLQuery().arrayVariableQueryV2(sparqlQueryWithStartAndEndYear, this.SPARQL_Q_GET_WIKIDATA_ARTWORK_IMGS_LIST_MULTIVAR, wikidataIDArr);
    return this.loadSparqlData(this.SPARQL_ENDPOINT_WIKIDATA, sparqlQueryWithVariable);
  }
  
  /**
    * @desc Load SPARQL data (portraits) from wikidata b/o wikidataID (using a QID)
    * @param ecarticoID {str}
    * @param resultFunction {function}
  */
  async loadArtistPortraitsSparqlData(personQID) {
    if (personQID == null) { return null };
    var sparqlQueryWithVariable = new SPARQLQuery().singleVariableQuery(this.SPARQL_Q_GET_WIKIDATA_ARTIST_PORTRAITS, personQID);
    return this.loadSparqlData(this.SPARQL_ENDPOINT_WIKIDATA, sparqlQueryWithVariable);
  }
}

export default LinkedDataWikidata;