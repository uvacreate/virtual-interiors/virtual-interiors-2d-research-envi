# Virtual Interiors 2D Research Environment Demonstrator

Repository for 2D map explorations for the Virtual Interiors project.

Demonstrator design/implementation: Hugo Huurdeman (https://orcid.org/0000-0002-3027-9597). Data & research on artists and art dealers: Weixuan Li (see also Li (2023; 2024) in [literature](#literature) section).

The final version of the demonstrator together with associated data on artists and art dealers has been published at the end of the Virtual Interiors project (2022). An updated version of the demonstrator has been published in June, 2024.

A __paper__ on the design of maps in a DH context (featuring this demonstrator) has been presented at DH Benelux 2024 and is available via [Zenodo](https://zenodo.org/records/11457262).

A __screencast__ illustrating the functionality of the prototype (v2) is available: [1080p video](https://app-data.virtualinteriorsproject.nl/screencasts/2d/202406_maps_prototype_demo_1080p.mp4) / [4k video](https://app-data.virtualinteriorsproject.nl/screencasts/2d/202406_maps_prototype_demo_4k.mp4).

**Try out** the demo via the link [below](#15-link-to-prototype).

## 1. General information

### 1.1 Background

Within [Virtual Interiors](https://www.virtualinteriorsproject.nl) (2018-2022), both 2D and 3D interfaces to big historical data were created and evaluated. 

In his work in designing and developing a research environment demonstrator for data related to historical maps, Hugo explored the following questions in a "co-design" setting with Weixuan Li ([PhD in Virtual Interiors](https://www.virtualinteriorsproject.nl/centres-of-creativity-in-amsterdam-of-the-dutch-golden-age/)) and Etienne Posthumus (at project partner Brill) :

- __How can [Linked Data](https://en.wikipedia.org/wiki/Linked_data) enhance historical map visualizations?__
  - The prototype uses biographical data as a "lynchpin" to connect to other sources, such as Wikidata and Adamnet. For the integration of this data, various hurdles had to be overcome (such as query efficiency and differences in used vocabularies). Ultimately, the prototype shows the power of linked Data by providing additional links, images and textual information, potentially useful for research purposes and potential serendipity. Further, the application prototype integrates various other open datasets, including map layers from Amsterdam Time Machine and streetplan layers from Adamnet. 
- __How to represent complex data within a map view? How to visualize data with varying degrees of uncertainty?__
  - This application aimed to integrate "data" and "map" displays in a unified interface. Maps and data points can be explored both in list and map modalities, and are directly linked with the temporal representation using a time slider. Imprecise temporal data is visualized by filtering time ranges instead of single years, and items without a known location can still be browsed via a list-based interface. Spatial uncertainty is visualized by adapting opacity. Adaptable color schemes are available (see e.g. MacEachren et al. [2005](https://doi.org/10.1109/TVCG.2012.279), [2012](https://doi.org/10.1559/1523040054738936))
- __How to show aggregated views of multiple map points?__
  - The prototype provides aggregated visual views. For textual, spatial and time-based selections of map points or polygons, it is possible to view visual galleries of all artworks in that selection. For each of these artworks, links to the underlying (linked) data can be followed.

This application uses the same user interface concepts and framework (Bootstrap) as the 3D research environment (see [Huurdeman & Piccoli, 2021](https://doi.org/10.1515/opar-2020-0142) for a detailed overview).

### 1.2 History

The 2D research environment was devised based on an initial assessment of researcher's needs and further co-design sessions with Weixuan Li and Etienne Posthumus. The first version of this interface was created in a short "sprint" with Etienne Posthumus using data from Brill Publishers and Amsterdam Time Machine (2019). Hugo designed and developed these initial demos further, with frequent sessions with Weixuan Li and input from Etienne (2020-2021). Code contributions by Ivan Kisjes and Saan Rashid (2022). In 2024, the demonstrator was revised and extended by Hugo Huurdeman (2024). Source code and documentation have been archived in [Zenodo](https://zenodo.org/records/5172687).

### 1.3 Features

- _Adaptive display_ of map points or polygons in a certain timerange
- _Time slider_ covering an adaptable number of years, e.g. 1, 10 or 25 years ("sliding window")
- Three types of _selectable layers_:
  - Map tile layers
    - From [Amsterdam Time Machine](https://tiles.amsterdamtimemachine.nl): [Van Berckenrode](https://images.huygens.knaw.nl/webmapper/maps/berckenrode/{z}/{x}/{y}.png) (1625), [De Broen](https://images.huygens.knaw.nl/webmapper/maps/debroen/{z}/{x}/{y}.png) (1724), [Publieke Werken](https://images.huygens.knaw.nl/webmapper/maps/pw-1909/{z}/{x}/{y}.png) (1909)
    - from ArcGIS: [Satellite view](http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/%7Bz%7D/%7By%7D/%7Bx%7D) (2020) 
  - Street plan layers 
    - From [Adamlink](https://adamlink.nl/data): [1625](https://adamlink.nl/data/geojson/streetsperyear/1625.json), [1700](https://adamlink.nl/data/geojson/streetsperyear/1700.json), [1909](https://adamlink.nl/data/geojson/streetsperyear/1909.json)
  - Data layers
    - Datasets by Weixuan Li, data P. Dijstelberge (see [Links to specific datasets](#links-to-specific-datasets))
    - Load custom datasets via URL parameters (see [URL_parameters](#url-parameters))
- _Linked data visualizations_
  - Biographical data (Ecartico), e.g. date of birth / date of death of a person
  - Links to external vocabularies, e.g. data.bibliotheken.nl, Getty, RKD (Ecartico)
  - Artworks by all people in selected range (Wikidata)
  - Artworks by a selected person (Wikidata)
  - Depictions of a selected person (Wikidata)
- Adaptable _uncertainty_ display
- Adaptable _palettes_ for items displayed on the map
- Possibility to display map items in the sidebar _with a known location_ (lat/lon or WKT polygon), or _without a known location_
- Ability to select _singular map items_, or _aggregated selections_ (see [Controls](#14-controls))
- Direct connection with _search and filtering_ feature in sidebar
  - Preserving context: also display results outside the current filter as white outlines
- **Possibility to do "Multi-queries" for multiple artists/art dealers, separated by a comma (2nd demonstrator version, June 2024)**
- Potentially usable for _other temporal, historical datasets_ (see [Input data structure](#31-input-data-structure))
- _Animation_ of map point distributions over time
- Possibility to _add annotations_ (currently only for the duration of the session)

This functionality was available in the first version of the demonstrator (v1.0), but not in the second version:
- _Experimental _3D map viewer_ (CesiumJS), directly connected to dataset displayed in 2D map_
- _Street views of a selected street (Adamlink)_

### 1.4 Controls

#### General controls:

The maps viewer allows for examining the main map (via the left panel) and additional information (via the sidebar panel). The right-most panel switches the sidebar contents between *General information*, an *Item list* with filters and the *Inspector* for data points (visible if a data point has been selected). The bar at the bottom of the interface allows for navigating the included years.

The top row of buttons contains basic UI options (full-screen, zoom) and controls for the maps, streetplans and data layers, as well as customizations for the shown year range and color palettes.

#### Additional controls:

- Select multiple map points (2D mode): `ctrl+click` (PC) or `cmd+click` (Mac). *Not possible in 3D mode*.

- Switch to 3D map view via `Tools` > `Toggle Cesium 3D Mode (experimental)`
  - Tilting map view (in 3D mode): `alt+click` (PC) or `option+click` (Mac)

### 1.5 Link to prototype

#### General link

- https://2d-demo.virtualinteriorsproject.nl (no dataset loaded)

#### Links to specific datasets:

- Currently supported datasets: Weixuan Li (artists / art dealers, 2022, included), Paul Dijstelberge (publishers, 2019). 

#### Loading custom data

Custom data can be loaded using the `data=` parameter. A dataset on a publicly accessible URL can be loaded. For instance, it is possible to load a dataset which has been "published to the internet" as a CSV in Google Sheets (https://support.google.com/docs/answer/183965). Copy the __full URL__ to the CSV, and add it after the `data=` parameter.

## 2. Customizing the application

### 2.1 URL parameters

Using URL parameters, i.e. parameters in the URL string, it is possible to adapt certain application settings

- e.g. `https://2d-demo.virtualinteriorsproject.nl/index.html ?` __`year=1650&start=1600&end=1700`__ loads the application, sets the *selected year* to 1650, the *start year* of the timeslider to 1600 and the *end year* to 1700.

URL parameters are separated with a `?` from the regular point of the URL; multiple parameters are separated using an `&`.

The following parameters are available in the Virtual Interiors 2D viewer:

#### Data settings

- `data=` : specify a publicly accessible URL, to a dataset in CSV format, with at least the columns specified above (*Data structure*), e.g. `data=data` (see also: [Loading custom data](#loading-custom-data))
- `label=` : specify a textual label for the dataset, e.g. `label=Dataset sample`. This will be visible in the user interface. Default: show the dataset URL.
- `lat=` : specify a custom latitude, e.g. `lat=52.37`. Default: `52.373078399` (Amsterdam view)
- `lon=` : specify a custom longitude, e.g. `lon=4.89`. Default: `4.892540077` (Amsterdam view)
- `zoom=` : specify an OpenLayers zoom level, e.g. `zoom=16`. Default: `14.8` (Amsterdam view)
- `rotation=` : specify a map rotation in degrees, e.g. `rotation=0`. Default:  Math.PI / 1.31 (Shows the drawings on the default map horizontally)

#### Year settings:

- `year=` : specify the default year in the map view and timeline, e.g. `year=1690`. Default: `1630`.
- `min=` : specify the first year in the timeline, e.g. `min=1550`. Default: `1572`.
- `max=` : specify the last year in the timeline, e.g. `max=1650`. Default: `1701`.
- `range=` : specify the default year range (in years), e.g. `range=15`. Default: `10`.
- `rate=` : specify the years per second (animation feature), determines animation speed, e.g. `rate=5` to slow the animation down. Default: `10`.

## 3. Data structure

### 3.1 Input data structure

This prototype currently reads CSV files as its input files (i.e. a file with comma-separated values). Each row of such a file represents one data point.

- The application __expects the following column names__ in the CSV data (* = required). Order of these is not important. Data points without a lat/lon value will only be represented in the sidebar.

#### ID representing the data point*:

  - __`person_id`__ : Ecartico ID [integer], e.g. `4580` ([link](https://www.vondel.humanities.uva.nl/ecartico/persons/4580)) OR: __`picarta_id`__ : picarta URL [string]

#### Details about the data point:

  - __`name`__* : name of the map point [string]
  - __`category`__* : category of the map point [string]
  - __`start_year`__* : first year the map point should be visible [YYYY, integer]
  - __`end_year`__* : last year the map point should be visible [YYYY, integer]
  - `address` : text representing address [string]
  - `processed_address` : processed address to match the current spelling / notation [string] (dataset P. Dijstelberge)
  - `occupation` : known occupations of person [string] (dataset P. Dijstelberge)

#### Location of data point:

  - `lon` : longitude [number], e.g. `4.898`
  - `lat` : latitude [number], e.g. `52.375`
  - `wkt` : wkt polygon [string], format: `POLYGON(([lon] [lat], [lon] [lat] ...))` or `MULTIPOLYGON ((([lon] [lat], [lon] [lat ...])))`. This is the format which is generated by the ["Wicket" tool](https://arthur-e.github.io/Wicket/sandbox-gmaps3.html).

If no location is provided in the data, the data point will only be visible in the sidebar (under *Results without location*).

## 4. Local use and development

Besides using the maps prototype via the provided link ([see above](#general-link)), it is possible to install and run it locally:

### 4.1 Installation

Needs `nodejs` (tested with v12.18 LTS), `npm` (tested with v6.14.4).

1. Clone the repository to a local folder
2. Using the command line, go to the `v1` folder in the cloned repository (`cd v1`) and run `npm install` to install the necessary modules (`npm update` to update to current version).
3. `npm start`: start the application (served at `localhost:3000`, which can be opened in a browser).

`npm run build` creates a standalone application build in the `/dist` folder, which can for instance be uploaded to a webserver.

### 4.2 Application structure

The application consists of the following structure:

#### Application sources

- `/` : main application files
  - `data` : all data files related to the application
    - `data/css` : CSS files (main style, Fontawesome files for icons)
    - `data/csv` : default CSV data files (can also be loaded externally)
    - `data/geojson` : default geoJSON files (source: adamlink.nl)
    - `data/images` : general application images/icons
    - `data/js` : various JS classes with application functionality (details: see [source code documentation](#45-source-code-documentation))
  - `jsdoc` : documentation generated by jsdoc

#### After running `npm install` :
  - `node modules` : used modules by the application

#### After running `npm run build` :
  - `dist` : the built application

### 4.3 Main dependencies

- OpenLayers (`ol`, v6.6.1) - 2D maps framework - https://www.openlayers.org
- Bootstrap (`bootstrap`, v4.6) - UI framework - https://getbootstrap.com/
- For 3D maps (1st demonstrator version): CesiumJS (`cesium`, v1.83.0) - 3D maps framework - https://cesium.com/platform/cesiumjs/
- For 3D maps (1st demonstrator version): ol-cesium (`ol-cesium`, v2.13.0) - Connective layer Openlayers-Cesium - https://openlayers.org/ol-cesium/

See [package.json](package.json) for all dependencies.

### 4.4 Setting a default dataset

To use a default dataset within the application, update `DEFAULT_DATASET_URL` in `main.js` to the URL of your dataset (relative or absolute). By default, no dataset is loaded within the application.

### 4.5 Source code documentation

Source code documentation is generated automatically via JSDoc, accessible via: https://2d-demo.virtualinteriorsproject.nl/jsdoc/.

### 4.6 Attributions

- This application uses Fontawesome icons (https://www.fontawesome.com)

### 4.5 Acknowledgements 

- During the development of the prototype, needed space on a web server was contributed by Brill Publishers/Etienne Posthumus
- Leon van Wissen for various support in using Linked Data, creating SPARQL queries, and CI/CD features

## 5. Known issues and limitations

### 5.1 Issues

- Annotations are only saved for the duration of a session (i.e. they will disappear after reloading the page).
- Enabling Cesium 3D maps might use more system resources, since various modules have to be loaded (even when just viewing the 2D maps). It is possible to turn these off (see [URL parameters](#2.1-url-parameters) above).
- Using labels for streetnames (from streetplan layer) creates visual issues and slowdown when switching to the 3D map. Therefore, if using the `enable3d=false` URL parameter, streetname labels are hidden except for close-range zoom levels.
- In the 3D mode, it is not possible to do aggregated selections of map points, due to technical limitations.
- For widespread deployment of this application, a caching mechanism for Linked Data queries would be preferred.

### 5.2 Limitations

- The Linked Data SPARQL queries (especially for Wikidata) had to be optimized to retrieve results within a short time (less than 10 seconds). Therefore, some fields (such as the year a painting was created) had to be left out of the query and cannot be shown directly be shown in the interface. However, the direct links to the original data make it possible to still view more details.

## 6. License 

This project is licensed under the GPL-v3 license. The full license can be found in [LICENSE](LICENSE).

## 7. Further information and resources

### Screencasts of demonstrator
- Screencast prototype v2, full dataset, June 2024: [1080p video](https://app-data.virtualinteriorsproject.nl/screencasts/2d/202406_maps_prototype_demo_1080p.mp4) / [4k video](https://app-data.virtualinteriorsproject.nl/screencasts/2d/202406_maps_prototype_demo_4k.mp4)
- Screencast prototype v1, partial dataset, Aug. 2021: [1080p video](https://app-data.virtualinteriorsproject.nl/screencasts/2d/202108_maps_prototype_demo.mp4) / [1080p HQ video](https://app-data.virtualinteriorsproject.nl/screencasts/2d/202108_maps_prototype_demo_hq.mp4)

### Presentations

- Guest lecture Hugo Huurdeman on webmapping (March 2021), https://www.slideshare.net/secret/aqpvFSwqUkuqwt
- Presentation Hugo Huurdeman at DH Benelux 2024 (June 2024), https://www.slideshare.net/slideshow/data-visualization-via-enhanced-maps-in-a-digital-humanities-context-a-design-perspective/269599400

### Literature
- Li, W. (2023). Painters’ playbooks: Deep mapping socio-spatial strategies in the art market of seventeenth-century
Amsterdam [PhD Thesis, University of Amsterdam]. https://hdl.handle.net/11245.1/bdaca776-71c2-4ff2-b5fa-12fbfabe1a5b
- Li, W. (2024). Deep Mapping Uncertain Historical Sources: Visualizing Business Knowledge of Painters in Seventeenth-
Century Amsterdam. Journal of Knowledge
- Huurdeman, H. C. (2024, June 5). Data Visualization via Enhanced Maps in a Digital Humanities Context – a Design Perspective. Digital Humanities Benelux 2024 Conference, Leuven, Belgium. https://doi.org/10.5281/zenodo.11457262

### Used SPARQL endpoints

- https://data.create.humanities.uva.nl/ 
- https://query.wikidata.org/
- https://druid.datalegend.net/AdamNet/all/sparql/endpoint#
